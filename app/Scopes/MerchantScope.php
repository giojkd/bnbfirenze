<?php


namespace App\Scopes;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Support\Facades\Auth;

class MerchantScope implements Scope
{

    public function apply(Builder $builder, Model $model){

        if (!in_array(Auth::user()->role,['user','admin','chef'])) {
            $builder->whereIn('merchant_id', Auth::user()->merchants->pluck('id'));
        }

    }

}
