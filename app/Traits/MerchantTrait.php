<?php

namespace App\Traits;
use Str;
use App;
use App\Models\Merchant;
use Illuminate\Support\Facades\Auth;
use Session;

trait MerchantTrait
{

    public function merchant(){
        return $this->belongsTo(Merchant::class);
    }

    public function getMerchant()
    {
        if (!Session::has('merchant_id')) {
            abort(404);
        }
        return Merchant::find(session('merchant_id'));
    }

}
