<?php

namespace App\Policies;

use App\Models\Merchant;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Laravel\Nova\Tests\Fixtures\Post as FixturesPost;
use Illuminate\Support\Facades\Auth;

class MerchantPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any posts.
     *
     * @param  \App\Models\User  $user
     * @return mixed
     */

    public function viewAny(User $user)
    {
        return true;
    }

    public function create(User $user){
        return $user->isAdmin();
    }

    public function delete(User $user)
    {
        return $user->isAdmin();
    }

    public function update(User $user,Merchant $merchant)
    {
        return ($user->merchants->pluck('id')->contains($merchant->id) || $user->isAdmin());
    }

}
