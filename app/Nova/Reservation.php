<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\HasMany;

use Eminiarts\Tabs\Tabs;
use Eminiarts\Tabs\TabsOnEdit;

use Titasgailius\SearchRelations\SearchesRelations;

use Maatwebsite\LaravelNovaExcel\Actions\DownloadExcel;
use App\Nova\Actions\PayOrders;

class Reservation extends Resource
{
    use TabsOnEdit, SearchesRelations;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Reservation::class;

    public static $group = 'Clienti';

    public static function label()
    {
        return __('Prenotazioni');
    }

    public static function singularLabel()
    {
        return __('Prenotazione');
    }

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */

    public function title()
    {
        return $this->code;
    }
    public function subtitle()
    {
        return optional($this->user)->name . ' ' . optional($this->user)->surname . ' - ' . optional($this->room)->name;
    }
    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'code'
    ];

    public static $searchRelations = [
        'user' => ['name', 'surname', 'email'],
        'room' => ['name'],
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [

            (new Tabs('Prenotazione', [

                'Info' => [

                    ID::make(__('ID'), 'id')->sortable(),

                    BelongsTo::make(__('Stanza'), 'room', Room::class)
                        ->nullable()
                        ->sortable(),

                    BelongsTo::make(__('Utente'), 'user', User::class)
                        ->nullable()
                        ->sortable(),

                    Text::make(__('Codice Prenotazione'), 'code')
                        ->sortable()
                        ->readonly()
                        ->hideWhenCreating()
                        // ->displayUsing(function() {
                        //     return '<strong>' . $this->code . '</strong>';
                        // })
                        ->asHtml(),

                    Date::make(__('Check-in'), 'arrival')
                        ->format('DD/MM/YYYY')
                        ->sortable()
                        ->rules('required'),

                    Date::make(__('Check-out'), 'departure')
                        ->format('DD/MM/YYYY')
                        ->sortable()
                        ->rules('required'),

                    Text::make('Durata', function() {
                        return count($this->daterange) . ' giorni';
                    })
                        ->hideFromIndex(),

                    Number::make(__('Adulti'), 'adults')
                        ->min(0)
                        ->max(4)
                        ->step(1)
                        ->hideFromIndex(),

                    Number::make(__('Bambini'), 'children')
                        ->min(0)
                        ->max(4)
                        ->step(1)
                        ->hideFromIndex(),

                    Number::make('Saldo', 'balance')
                        ->min(0)
                        ->max(999)
                        ->step(0.5)
                        ->sortable()
                        ->hideWhenCreating()
                        // ->displayUsing(function() {
                        //     return '<strong>' . number_format($this->price, 2, ',', '.') . ' €</strong>';
                        // })
                        ->asHtml(),

                    Boolean::make('GDPR', 'gdpr')
                        ->hideFromIndex(),

                    Boolean::make('Confermato', 'confirmed')
                        ->sortable(),

                    Boolean::make('Pagato', 'paid')
                        ->sortable(),
                    BelongsTo::make(__('Merchant'), 'merchant', Merchant::class),#->nullable(),

                ],

                'Tracking' => [

                    Text::make(__('Sorgente'), 'source')
                        ->hideFromIndex(),

                    Text::make(__('Mezzo'), 'medium')
                        ->hideFromIndex(),

                    Text::make(__('Campagna'), 'campaign')
                        ->hideFromIndex(),

                    Text::make(__('Contenuto'), 'content')
                        ->hideFromIndex(),

                ]

            ]))->withToolbar(),

            HasMany::make(__('Ordini'), 'orders', Order::class),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [
            (new DownloadExcel)
                ->withHeadings()
                ->allFields(),
            (new PayOrders)
                ->showOnTableRow(),
        ];
    }
}
