<?php

namespace App\Nova\Actions;

use Illuminate\Bus\Queueable;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Actions\Action;
use Illuminate\Support\Collection;
use Laravel\Nova\Fields\ActionFields;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class PayOrders extends Action
{
    use InteractsWithQueue, Queueable;

    // public $name = "Stato pagamento";
    public $name = "Pagato";

    /**
     * Perform the action on the given models.
     *
     * @param  \Laravel\Nova\Fields\ActionFields  $fields
     * @param  \Illuminate\Support\Collection  $models
     * @return mixed
     */
    public function handle(ActionFields $fields, Collection $models)
    {
        foreach ($models as $model)
        {
            // $model->paid = $fields->paid;
            $model->paid = true;
            $model->save();
        }
    }

    /**
     * Get the fields available on the action.
     *
     * @return array
     */
    public function fields()
    {
        return [
            // Select::make('Stato', 'paid')
            //     ->options([
            //         true => 'Pagato',
            //         false => 'Non pagato',
            //     ])
            //     ->displayUsingLabels(),
        ];
    }
}
