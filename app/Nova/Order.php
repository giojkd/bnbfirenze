<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;

use Titasgailius\SearchRelations\SearchesRelations;

class Order extends Resource
{
    use SearchesRelations;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Order::class;

    public static $group = 'Clienti';

    public static function label()
    {
        return __('Ordini');
    }

    public static function singularLabel()
    {
        return __('Ordine');
    }

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'code';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'code'
    ];

    public static $searchRelations = [
        'reservation' => ['code'],
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            BelongsTo::make(__('Prenotazione'), 'reservation', Reservation::class)
                ->sortable(),

            Text::make(__('Identificativo'), 'code')
                ->hideWhenCreating()
                ->readonly()
                ->sortable(),

            Date::make(__('Giorno'), 'day')
                ->format('DD/MM/YYYY')
                ->sortable()
                ->rules('required'),

            Number::make('Prezzo', 'total')
                ->min(0)
                ->max(999)
                ->step(0.5)
                ->sortable(),

            Select::make(__('Slot'), 'slot')
                ->options([
                    '7:40' => 'Ore 7:40',
                    '8:00' => 'Ore 8:00',
                    '8:20' => 'Ore 8:20',
                    '8:40' => 'Ore 8:40',
                    '9:00' => 'Ore 9:00',
                    '9:20' => 'Ore 9:20',
                    '9:40' => 'Ore 9:40',
                    '10:00' => 'Ore 10:00',
                    '10:20' => 'Ore 10:20',
                    '10:40' => 'Ore 10:40',
                    '11:00' => 'Ore 11:00',
                ])
                ->displayUsingLabels(),

            Boolean::make(__('In camera'), 'room_service')
                ->sortable(),

            Boolean::make(__('Pronto'), 'ready')
                ->sortable(),

            BelongsToMany::make(__('Piatti'), 'recipes', Recipe::class)
                ->fields(function () {
                    return [
                        Text::make(__('Quantità'), 'quantity'),
                    ];
                }),

            BelongsTo::make(__('Merchant'), 'merchant', Merchant::class),#->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
