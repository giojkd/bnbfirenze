<?php

namespace App\Nova;

use Laravel\Nova\Resource;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Waynestate\Nova\CKEditor;
use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\HasMany;
use Laravel\Nova\Fields\MorphMany;

class Room extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Room::class;

    public static $group = 'Gestionale';

    public static function label()
    {
        return __('Stanze');
    }

    public static function singularLabel()
    {
        return __('Stanza');
    }

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            Text::make(__('Nome'), 'name')
                ->sortable()
                ->rules('required'),

            Images::make('Foto', 'gallery')
                ->conversionOnIndexView('thumb')
                ->enableExistingMedia(),

            CKEditor::make(__('Descrizione'), 'description')
                ->alwaysShow()
                ->hideFromIndex(),

            Boolean::make(__('Visibile'), 'published')
                ->sortable(),

            HasMany::make(__('Prenotazioni'), 'reservations', Reservation::class),

            MorphMany::make(__('Traduzioni'), 'translations', Translation::class),

            BelongsTo::make(__('Merchant'), 'merchant', Merchant::class),#->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
