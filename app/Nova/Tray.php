<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\BelongsToMany;

use Titasgailius\SearchRelations\SearchesRelations;

class Tray extends Resource
{
    use SearchesRelations;

    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Tray::class;

    public static $group = 'Clienti';

    public static function label()
    {
        return __('Vassoi');
    }

    public static function singularLabel()
    {
        return __('Vassoio');
    }

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    public static $displayInNavigation = false;

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name'
    ];

    public static $searchRelations = [
        'reservation' => ['code'],
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            BelongsTo::make(__('Prenotazione'), 'reservation', Reservation::class)
                ->sortable(),

            BelongsTo::make(__('Menu'), 'menu', Menu::class)
                ->nullable()
                ->hideFromIndex(),

            Text::make(__('Nome'), 'name')
                ->sortable(),

            Date::make(__('Giorno'), 'day')
                ->format('DD/MM/YYYY')
                ->sortable()
                ->rules('required'),

            Number::make('Prezzo', 'total')
                ->min(0)
                ->max(999)
                ->step(0.5)
                ->sortable(),

            BelongsToMany::make(__('Piatti'), 'recipes', Recipe::class),

            BelongsTo::make(__('Merchant'), 'merchant', Merchant::class),#->nullable(),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
