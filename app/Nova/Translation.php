<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Waynestate\Nova\CKEditor;
use Laravel\Nova\Fields\MorphTo;
use Laravel\Nova\Fields\Select;

use Jackabox\DuplicateField\DuplicateField;

use App\Nova\Filters\TranslationTypeFilter;
use App\Nova\Filters\TranslationLangFilter;

class Translation extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Translation::class;

    public static $group = 'Multilingua';

    public static function label()
    {
        return __('Traduzioni');
    }

    public static function singularLabel()
    {
        return __('Traduzione');
    }

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'title';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'title'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            MorphTo::make(__('Traduzione'), 'translatable')
                ->types([
                    Category::class,
                    Recipe::class,
                    Menu::class,
                    Room::class,
                ]),

            Select::make(__('Lingua'), 'lang')
                ->options([
                    'it' => 'Italiano',
                    'en' => 'English',
                ])
                ->displayUsingLabels()
                ->sortable()
                ->rules('required'),

            Text::make(__('Titolo'), 'title')
                ->sortable()
                ->rules('required'),

            CKEditor::make(__('Contenuto'), 'content')
                ->alwaysShow()
                ->hideFromIndex(),

            DuplicateField::make('Duplicate')
                ->withMeta([
                    'resource' => 'translations', // resource url
                    'model' => 'App\Models\Translation', // model path
                    'id' => $this->id, // id of record
                ])
                ->canSee(function ($request) {
                    return $request->user()->isAdmin();
                }),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [
            (new TranslationTypeFilter),
            (new TranslationLangFilter)
        ];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
