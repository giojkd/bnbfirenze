<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Gravatar;
use Laravel\Nova\Fields\Password;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\HasOne;
use Laravel\Nova\Fields\HasMany;

use KABBOUCHI\NovaImpersonate\Impersonate;
use Laravel\Nova\Fields\BelongsToMany;

class User extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\User::class;

    public static $group = 'Database';

    public static function label()
    {
        return __('Utenti');
    }

    public static function singularLabel()
    {
        return __('Utente');
    }

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public function title()
    {
        return $this->name . ' ' . $this->surname;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name', 'surname', 'email', 'phone'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make()->sortable(),

            Gravatar::make()->maxWidth(50),

            Text::make(__('Nome'), 'name')
                ->sortable()
                ->rules('required', 'max:255'),

            BelongsToMany::make(__('Merchant'), 'merchants', Merchant::class),

            Text::make(__('Cognome'), 'surname')
                ->sortable()
                ->rules('required', 'max:255'),

            Text::make(__('Email'), 'email')
                ->sortable()
                ->rules('required', 'email', 'max:254')
                ->creationRules('unique:users,email')
                ->updateRules('unique:users,email,{{resourceId}}'),

            Text::make(__('Telefono'), 'phone')
                ->sortable()
                ->rules('max:15'),

            Select::make(__('Ruolo'), 'role')
                ->options([
                        "admin" => 'Admin',
                        "chef" => 'Chef',
                        "user" => 'Utente',
                    ])
                ->displayUsingLabels()
                ->sortable()
                ->canSee(function($request) {
                    return $request->user()->isAdmin();
                }),



            Password::make(__('Password'), 'password')
                ->onlyOnForms()
                ->creationRules('required', 'string', 'min:8')
                ->updateRules('nullable', 'string', 'min:8'),

            HasOne::make(__('Profilo'), 'profile', Profile::class),

            HasMany::make(__('Prenotazioni'), 'reservations', Reservation::class),

            Impersonate::make($this)
                ->canSee(function($request) {
                    return $request->user()->isAdmin();
                }),



        ];
    }

    public static function indexQuery(NovaRequest $request, $query)
    {
        if($request->user()->isAdmin())
            return $query;

        return $query->where('users.id', $request->user()->id);
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
