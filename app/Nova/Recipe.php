<?php

namespace App\Nova;

use Illuminate\Http\Request;
use Laravel\Nova\Http\Requests\NovaRequest;

use Laravel\Nova\Fields\ID;
use Laravel\Nova\Fields\Text;
use Ebess\AdvancedNovaMediaLibrary\Fields\Images;
use Waynestate\Nova\CKEditor;
use Laravel\Nova\Fields\Number;
use Laravel\Nova\Fields\Boolean;
use Laravel\Nova\Fields\BelongsTo;

use Laravel\Nova\Fields\BelongsToMany;
use Laravel\Nova\Fields\MorphMany;

class Recipe extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Recipe::class;

    public static $group = 'Gestionale';

    public static $perPageViaRelationship = 20;

    public static function label()
    {
        return __('Piatti');
    }

    public static function singularLabel()
    {
        return __('Piatto');
    }

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public static $title = 'name';

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'name',
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            BelongsTo::make(__('Categoria'), 'category', Category::class)
                ->sortable(),

            Text::make(__('Tipo'), 'type')
                ->readOnly()
                ->hideFromIndex(),

            Images::make('Foto', 'gallery')
                    ->conversionOnIndexView('thumb')
                    ->enableExistingMedia(),

            Text::make(__('Nome'), 'name')
                ->sortable()
                ->rules('required'),

            CKEditor::make(__('Descrizione'), 'description')
                ->alwaysShow()
                ->hideFromIndex(),

            Number::make('Prezzo (€)', 'price')
                ->min(0)
                ->step(0.5)
                ->sortable()
                ->rules('required'),

            Boolean::make(__('Con Stella'), 'starred')
                ->sortable(),

            Boolean::make(__('Visibile'), 'published')
                ->sortable(),

            BelongsToMany::make(__('Menu'), 'menus', Menu::class),

            BelongsToMany::make(__('Vassoi'), 'trays', Tray::class)
                ->hideFromDetail(),

            BelongsToMany::make(__('Ordini'), 'orders', Order::class)
                ->fields(function () {
                    return [
                        Text::make(__('Quantità'), 'quantity'),
                    ];
                })
                ->hideFromDetail(),

            MorphMany::make(__('Traduzioni'), 'translations', Translation::class),
            BelongsTo::make(__('Merchant'), 'merchant', Merchant::class),#->nullable(),
        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
