<?php

namespace App\Nova;

use Laravel\Nova\Fields\ID;
use Illuminate\Http\Request;
use Laravel\Nova\Fields\BelongsTo;
use Laravel\Nova\Fields\Text;
use Laravel\Nova\Fields\Select;
use Laravel\Nova\Fields\Country;
use Laravel\Nova\Fields\Date;
use Laravel\Nova\Fields\Place;
use Laravel\Nova\Http\Requests\NovaRequest;

class Profile extends Resource
{
    /**
     * The model the resource corresponds to.
     *
     * @var string
     */
    public static $model = \App\Models\Profile::class;

    public static $group = 'Database';

    public static function label()
    {
        return __('Profili');
    }

    public static function singularLabel()
    {
        return __('Profilo');
    }

    /**
     * The single value that should be used to represent the resource when being displayed.
     *
     * @var string
     */
    public function title()
    {
        return $this->name . ' ' . $this->surname;
    }

    /**
     * The columns that should be searched.
     *
     * @var array
     */
    public static $search = [
        'id', 'fiscal_code'
    ];

    /**
     * Get the fields displayed by the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function fields(Request $request)
    {
        return [
            ID::make(__('ID'), 'id')->sortable(),

            BelongsTo::make(__('Utente'), 'user', User::class),

            Place::make(__('Indirizzo'), 'address')
                ->postalCode('cap')
                ->city('city')
                ->country('country')
                ->hideFromIndex(),

            Text::make(__('CAP'), 'cap')
                ->hideFromIndex(),

            Place::make(__('Città'), 'city')
                ->sortable(),

            Text::make(__('Provincia'), 'province')
                ->sortable(),

            // Select::make(__('Regione'), 'region')
            //     ->options([
            //         "abruzzo" => "Abruzzo",
            //         "basilicata" => "Basilicata",
            //         "calabria" => "Calabria",
            //         "campania" => "Campania",
            //         "emilia-romagna" => "Emilia Romagna",
            //         "friuli-venezia-giulia" => "Friuli Venezia Giulia",
            //         "lazio" => "Lazio",
            //         "liguria" => "Liguria",
            //         "lombardia" => "Lombardia",
            //         "marche" => "Marche",
            //         "molise" => "Molise",
            //         "piemonte" => "Piemonte",
            //         "puglia" => "Puglia",
            //         "sardegna" => "Sardegna",
            //         "sicilia" => "Sicilia",
            //         "toscana" => "Toscana",
            //         "provincia-autonoma-di-bolzano" => "Provincia autonoma di Bolzano",
            //         "provincia-autonoma-di-trento" => "Provincia autonoma di Trento",
            //         "umbria" => "Umbria",
            //         "valle-d-aosta" => "Valle d'Aosta",
            //         "veneto" => "Veneto",
            //     ])
            //     ->displayUsingLabels()
            //     ->sortable(),

            Country::make(__('Nazione'), 'country')
                ->hideFromIndex(),

            Date::make(__('Data di nascita'), 'birthdate')
                ->format('DD/MM/YYYY')
                ->hideFromIndex(),

            Text::make(__('Ragione sociale'), 'company_name')
                ->hideFromIndex(),
            
            Text::make(__('P.IVA'), 'vat_number')
                ->hideFromIndex(),
                
            Text::make(__('PEC o SDI'), 'pec_sdi')
                ->hideFromIndex(),
                
            Text::make(__('Cod. fisc.'), 'fiscal_code')
                ->sortable()
                ->displayUsing(function(){
                    return strtoupper($this->fiscal_code);
                }),

        ];
    }

    /**
     * Get the cards available for the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function cards(Request $request)
    {
        return [];
    }

    /**
     * Get the filters available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function filters(Request $request)
    {
        return [];
    }

    /**
     * Get the lenses available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function lenses(Request $request)
    {
        return [];
    }

    /**
     * Get the actions available for the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function actions(Request $request)
    {
        return [];
    }
}
