<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Redirects
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        #if(count(explode('.', $request->getHost())) < 3){ return redirect('https://www.leylasclub.com'. $request->path() . ($request->getQueryString() ? '?' . $request->getQueryString() : '')); }
        return $next($request);

    }
}
