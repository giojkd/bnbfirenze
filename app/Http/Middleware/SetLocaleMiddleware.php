<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class SetLocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $lang = session()->get('chooselanguage', 'it');
        App::setLocale($lang);
        if ($lang == 'it') {
            setlocale(LC_ALL, 'it_IT.utf8', 'it_IT', 'it');
        } elseif($lang == 'en') {
            setlocale(LC_ALL, 'en_US.utf8', 'en_US', 'en');
        } else {
            setlocale(LC_ALL, $lang);
        }
        
        return $next($request);
    }
}
