<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class StoreUTMinCookie
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $utm_source = $request->input('utm_source');
        $utm_medium = $request->input('utm_medium');
    	$utm_campaign = $request->input('utm_campaign');
        $utm_content = $request->input('utm_content');

    	if($utm_source || $utm_medium || $utm_campaign || $utm_content){

    		$utm  = [
    			"source"			=> $utm_source,
                "medium"            => $utm_medium,
    			"campaign"			=> $utm_campaign,
                "content"			=> $utm_content,
    			"click_date"	    => date("Y-m-d H:i:s")
    		];

            // I campi riempibili sono i seguenti: ->cookie($name, $value, $minutes, $path, $domain, $secure, $httpOnly)
            
            $minutes = 8400;
            
            // Nelle versioni più aggiornate di Laravel non è più possibile passare il contenuto del cookie come array, ma solo come stringa, quindi si può usare json_encode e poi json_decode per il contenuto del cookie
            return $next($request)->cookie('utm', json_encode($utm), $minutes);

    	}

        return $next($request);
    }
}
