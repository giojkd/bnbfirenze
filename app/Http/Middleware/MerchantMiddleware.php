<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App;
use App\Models\Merchant;
use Config;
use Illuminate\Support\Facades\URL;

class MerchantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {



        if (is_null($request->account)) {
            abort('404');
        }else{

            $merchant = Merchant::where('domain_name',$request->account)->first();

            if(is_null($merchant)){
                abort(404);
            }

            Session::put('merchant_id', $merchant->id);
            Session::put('account', $merchant->domain_name);

            URL::defaults(['account' => $merchant->domain_name]);

            return $next($request);

        }


    }
}
