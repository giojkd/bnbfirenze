<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use App\Models\Order;
use Livewire\Component;

class DailyOrders extends Component
{
    public $orders;
    public $dailyOrders;
    public $readyOrders;

    public $slotArray;

    public function mount()
    {
        $this->dailyOrders = collect();
        $this->readyOrders = collect();
        $this->orders = Order::where('day', Carbon::today())
                             ->orWhere('day', Carbon::today()->addDay(1))
                             ->orderBy('day', 'desc')
                             ->get();
        $this->slotArray = [];
        foreach ($this->orders as $order) {
            $this->slotArray[$order->id] = intval(str_replace(':', '', $order->day->format('m:d'))) . intval(str_replace(':', '', $order->slot));
        }
        asort($this->slotArray);
        foreach ($this->slotArray as $key => $value) {
            $this->dailyOrders->push(Order::find($key));
            $this->readyOrders->push(Order::find($key));
        }

        $this->dailyOrders = $this->dailyOrders->where('ready', false);
        $this->readyOrders = $this->readyOrders->where('ready', true);
    }

    public function readyOrder($order_id)
    {
        Order::find($order_id)->update([
            'ready' => true,
        ]);
        asort($this->slotArray);
        $this->dailyOrders = collect();
        $this->readyOrders = collect();
        foreach ($this->slotArray as $key => $value) {
            $this->dailyOrders->push(Order::find($key));
            $this->readyOrders->push(Order::find($key));
        }

        $this->dailyOrders = $this->dailyOrders->where('ready', false);
        $this->readyOrders = $this->readyOrders->where('ready', true);
    }

    public function render()
    {
        return view('livewire.daily-orders');
    }
}
