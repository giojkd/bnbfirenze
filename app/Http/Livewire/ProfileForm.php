<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

class ProfileForm extends Component
{   
    public $profile;
    public $user_id;
    public $address;
    public $cap;
    public $city;
    public $region;
    public $province;
    public $country;
    public $birthdate;
    public $fiscal_code;
    public $company_name;
    public $vat_number;
    public $pec_sdi;

    public $successMessage;

    public $invoice = false;
    
    protected $rules = [
        'address' => 'nullable',
        'cap' => 'nullable',
        'city' => 'nullable',
        'province' => 'nullable',
        'region' => 'nullable',
        'country' => 'nullable',
        'birthdate' => 'nullable',
        'fiscal_code' => 'min:16|nullable',
        'fiscal_code' => 'min:16|nullable',
        'company_name' => 'nullable',
        'vat_number' => 'nullable',
        'pec_sdi' => 'nullable',
    ];

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function submitForm(Request $request)
    {
        
        $profile = $this->validate();

        $profile = Profile::create([
            'user_id' => Auth::user()->id,
            'address' => $this->address,
            'cap' => $this->cap,
            'city' => $this->city,
            'region' => $this->region,
            'province' => $this->province,
            'country' => $this->country,
            'birthdate' => $this->birthdate,
            'fiscal_code' => $this->fiscal_code,
            'company_name' => $this->company_name,
            'vat_number' => $this->vat_number,
            'pec_sdi' => $this->pec_sdi,
        ]);

        // $this->resetForm();

        $this->successMessage = 'Profilo completato con successo';
    }

    private function resetForm()
    {
        $this->address = NULL;
        $this->cap = NULL;
        $this->city = NULL;
        $this->region = NULL;
        $this->province = NULL;
        $this->country = NULL;
        $this->birthdate = NULL;
        $this->fiscal_code = NULL;
        $this->company_name = NULL;
        $this->vat_number = NULL;
        $this->pec_sdi = NULL;
    }

    public function render()
    {
        return view('livewire.profile-form');
    }
}
