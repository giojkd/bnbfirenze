<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use App\Models\Room;
use Livewire\Component;
use App\Models\Reservation;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Mail\ReservationMessage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cookie;
use App\Traits\MerchantTrait;

class ReservationForm extends Component
{

    use MerchantTrait;

    public $arrival;
    public $departure;
    public $adults;
    public $children;
    public $user_id;
    public $room_id;
    public $gdpr;

    protected $rules = [
        'arrival' => 'required',
        'departure' => 'required',
        'adults' => 'required',
        'room_id' => 'required',
        'gdpr' => 'required',
    ];

    public function mount (Request $request)
    {
        $this->user_id = Auth::user() ? Auth::user()->id : null;
        $this->room_id = $request->room_id;
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function checkAvailability($arrival_date, $departure_date, $room_id)
    {
        $booked_dates = Reservation::where('room_id', $room_id)
                                    #->where('confirmed', true)
                                    ->whereDate('arrival', '>=', Carbon::today())
                                    ->get()
                                    ->pluck('daterange')
                                    ->toArray();

        for ($i=0; $i < Carbon::parse($arrival_date)->diffInDays(Carbon::parse($departure_date)); $i++) {
            $currentDate = Carbon::parse($arrival_date)->addDays($i);
            if(in_array($currentDate->format('Y-m-d'), Arr::flatten($booked_dates))) {
                return false;
            }
        }

        return true;
    }

    public function submitForm(Request $request)
    {



        $reservation = $this->validate();

        $merchant = $this->getMerchant();


        if($this->checkAvailability($this->arrival, $this->departure, $this->room_id))
        {
            $cookieUtm = json_decode($request->cookie('utm'));

            $reservation = Reservation::create([
                'arrival' => $this->arrival,
                'departure' => $this->departure,
                'adults' => $this->adults,
                'children' => $this->children ?: 0,
                'user_id' => $this->user_id,
                'room_id' => $this->room_id,
                'gdpr' => $this->gdpr,
                'source' => $cookieUtm === null ? "Direct" : $cookieUtm->source,
                'medium' => $cookieUtm === null ? "Direct" : $cookieUtm->medium,
                'campaign' => $cookieUtm === null ? "Direct" : $cookieUtm->campaign,
                'content' => $cookieUtm === null ? "Direct" : $cookieUtm->content,
                'merchant_id' => $merchant->id

            ]);

            Cookie::forget('utm');

            Mail::to('info@leylasclub.com')->send(new ReservationMessage($reservation));

            return redirect()->route('thank-you',['account' => session('account')]);
        }

        session()->flash('message', 'Stanza non disponibile in queste date');

        return redirect()->back();

    }

    public function render()
    {
        return view('livewire.reservation-form');
    }
}
