<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use App\Models\Menu;
use App\Models\Order;
use App\Models\Recipe;
use Livewire\Component;
use App\Models\Category;
use App\Models\Tray;
use App\Traits\MerchantTrait;
use Illuminate\Support\Facades\Auth;

class MenuComponent extends Component
{

    use MerchantTrait;

    public $reservation;
    public $menu;
    public $categories;
    public $balance = 0;
    public $stars = 0;
    public $types;
    public $recipes;

    public $storiesRecipe;
    public $key;

    public function mount(Menu $menu)
    {
        $this->menu = $menu;
        $this->reservation = Auth::user()->reservations()
                                         ->whereDate('arrival', '<=', Carbon::today())
                                         ->whereDate('departure', '>', Carbon::today())
                                         ->where('confirmed', true)
                                         ->first();
        if ($this->reservation) {
            $this->balance = 0 - $this->reservation->startingBalance;
            $this->stars = $this->reservation->startingStars;
        }
        $this->categories = Category::where('published', true)->with('recipes')->get();
        $this->types = $menu->recipes->pluck('type')->unique();
        $this->recipes = [];
        $this->storiesRecipe = [];
    }

    public function addRecipe($recipe_id)
    {
        $recipe = Recipe::find($recipe_id);

        $this->recipes[] = $recipe->id;
        if($recipe->starred && $this->stars > 0) {
            $this->storiesRecipe[array_key_last($this->recipes)] = 'star';
            $this->stars = $this->stars - 1;
        } else {
            $this->storiesRecipe[array_key_last($this->recipes)] = 'balance';
            $this->balance = $this->balance + $recipe->price;
        }
    }
    public function removeRecipe($recipe_id)
    {
        $recipe = Recipe::find($recipe_id);
        if (in_array($recipe_id, $this->recipes)) {
            unset($this->recipes[array_search($recipe->id, $this->recipes)]);
            $key = array_diff_key($this->storiesRecipe, $this->recipes);
            if ($this->storiesRecipe[array_key_first($key)] == 'star') {
                unset($this->storiesRecipe[array_key_first($key)]);
                $this->stars = $this->stars + 1;
            } else {
                unset($this->storiesRecipe[array_key_first($key)]);
                $this->balance = $this->balance - $recipe->price;
            }


            // if($recipe->starred && $this->stars < $this->reservation->startingStars) {
            //     $this->stars = $this->stars + 1;
            // } elseif((- $this->balance) >= $this->reservation->startingBalance) {
            //     $this->balance = - $this->reservation->startingBalance;
            // } else {
            //     $this->balance = $this->balance - $recipe->price;
            // }
        }
    }

    public function addRecipeFixedMenu($recipe_id)
    {
        $recipe = Recipe::find($recipe_id);
        $this->recipes[$recipe->type] = $recipe->id;
        $this->balance = $this->menu->price;
    }

    public function newTray($menu_id)
    {
        // Nel caso di menu fissi, controllo che ci siano 4 piatti selezionati
        // Nel caso del menu alla carta, controllo che ne sia selezionato almeno 1

        $merchant = $this->getMerchant();

        if($this->menu->price != 0 && count($this->recipes) != 4) {
            session()->flash('message', __('Seleziona un piatto in ciascuna sezione'));
            return redirect()->back();
        } elseif(count($this->recipes) == 0) {
            session()->flash('message', __('Seleziona almeno un piatto'));
            return redirect()->back();
        }

        // Creo il vassoio
        $tray = Tray::create([
            'reservation_id' => $this->reservation->id,
            'menu_id' => $menu_id,
            'name' => $this->menu->name,
            'total' => $this->balance > 0 ? $this->balance : 0,
            'day' => Carbon::today()->addDays(1),
        ]);

        $tray->recipes()->attach($this->recipes);

        return redirect()->route('user.orders',['account' => $merchant->domain_name]);
    }

    public function render()
    {
        return view('livewire.menu-component');
    }
}
