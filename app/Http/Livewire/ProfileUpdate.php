<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Http\Request;
use App\Models\Profile;
use Illuminate\Support\Facades\Auth;

class ProfileUpdate extends Component
{   
    public $profile;
    public $user_id;
    public $address;
    public $cap;
    public $city;
    public $region;
    public $province;
    public $country;
    public $birthdate;
    public $fiscal_code;
    public $company_name;
    public $vat_number;
    public $pec_sdi;

    public $updateMessage;
   
    protected $rules = [
        'address' => 'nullable',
        'cap' => 'nullable',
        'city' => 'nullable',
        'province' => 'nullable',
        'region' => 'nullable',
        'country' => 'nullable',
        'birthdate' => 'nullable',
        'fiscal_code' => 'min:16|nullable',
        'fiscal_code' => 'min:16|nullable',
        'company_name' => 'nullable',
        'vat_number' => 'nullable',
        'pec_sdi' => 'nullable',
    ];

    public function mount()
    {
        $profile = Profile::where('user_id', Auth::user()->id)->first();
        $this->profile = $profile;

        $this->address = $profile->address == null ? null : $profile->address ;
        $this->cap = $profile->cap == null ? null : $profile->cap ;
        $this->city = $profile->city == null ? null : $profile->city ;
        $this->region = $profile->region == null ? null : $profile->region ;
        $this->province = $profile->province == null ? null : $profile->province ;
        $this->country = $profile->country == null ? null : $profile->country ;
        $this->birthdate = $profile->birthdate == null ? null : $profile->birthdate ;
        $this->fiscal_code = $profile->fiscal_code == null ? null : $profile->fiscal_code ;
        $this->company_name = $profile->company_name == null ? null : $profile->company_name ;
        $this->vat_number = $profile->vat_number == null ? null : $profile->vat_number ;
        $this->pec_sdi = $profile->pec_sdi == null ? null : $profile->pec_sdi ;
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }

    public function formUpdate(Request $request)
    {
        
        $profile = $this->validate();
        
        $this->profile->update([
            'address' => $this->address,
            'cap' => $this->cap,
            'city' => $this->city,
            'region' => $this->region,
            'province' => $this->province,
            'country' => $this->country,
            'birthdate' => $this->birthdate,
            'fiscal_code' => $this->fiscal_code,
            'company_name' => $this->company_name,
            'vat_number' => $this->vat_number,
            'pec_sdi' => $this->pec_sdi,
            ]);
        $this->profile->save();
    
        $this->updateMessage = 'Profilo completato con successo';
    }

    public function render()
    {
        return view('livewire.profile-update');
    }
}
