<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Reservation;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReservationMessage;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class FormController extends Controller
{
    private $rules = array(
        'arrival' => 'required',
        'departure' => 'required',
        'adults' => 'required',
        'children' => 'required',
        'room_id' => 'required',
        'gdpr' => 'required',
    );

    private function submitForm($request)
    {
        $cookieUtm = json_decode($request->cookie('utm'));

        $arrival = $request->input('arrival');
        $departure = $request->input('departure');
        $adults = $request->input('adults');
        $children = $request->input('children');
        $user_id = Auth::user()->id;
        $room_id = $request->input('room_id');
        $gdpr = $request->input('gdpr');
        $source = $cookieUtm === null ? "Direct" : $cookieUtm->source;
        $medium = $cookieUtm === null ? "Direct" : $cookieUtm->medium;
        $campaign = $cookieUtm === null ? "Direct" : $cookieUtm->campaign;
        $content = $cookieUtm === null ? "Direct" : $cookieUtm->content;

        // $token = $request->input('token');
        // $action = $request->input('action');

        // if(!$this->verifyRecaptcha($token, $action))
        //     return redirect()->back();

        $reservation = Reservation::create([
            'arrival' => $arrival,
            'departure' => $departure,
            'adults' => $adults,
            'children' => $children,
            'user_id' => $user_id,
            'room_id' => $room_id,
            'gdpr' => $gdpr,
            'source' => $source,
            'medium' => $medium,
            'campaign' => $campaign,
            'content' => $content,
        ]);

        Mail::to('info@leylasclub.com')->queue(
            (new ReservationMessage($reservation))
        );

        // Thank you page
        return redirect('/grazie')->withCookie(Cookie::forget('utm'));
    }

    public function contactForm(Request $request) {

        $validator = Validator::make($request->all(), $this->rules);

        if ($validator->fails()){
            return back()->with('error', 'form-non-compilato');
        }
       
        return $this->submitForm($request);
    }

    // private function verifyRecaptcha($token, $action)
    // {
    //     if(config('app.env') == 'local'){
    //         return true;
    //     }

    //     $ch = curl_init();
    //     curl_setopt($ch, CURLOPT_URL,"https://www.google.com/recaptcha/api/siteverify");
    //     curl_setopt($ch, CURLOPT_POST, 1);
    //     curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(array('secret' => config('services.recaptcha.secret_key'), 'response' => $token)));
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //     $response = curl_exec($ch);
    //     curl_close($ch);
    //     $arrResponse = json_decode($response, true);

    //     if($arrResponse["success"] == '1' && $arrResponse["action"] == $action && $arrResponse["score"] >= 0.5) {
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
}
