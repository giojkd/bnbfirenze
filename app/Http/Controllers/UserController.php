<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Carbon\Carbon;
use App\Models\Tray;
use App\Models\Order;
use App\Models\Reservation;
use App\Traits\MerchantTrait;

class UserController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */

    use MerchantTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function restrictedArea()
    {

        $user = Auth::user()->load('profile', 'reservations.room', 'reservations.orders');
        $currentReservation = $user->reservations()->whereDate('arrival', '<=', Carbon::today())->whereDate('departure', '>=', Carbon::today())->where('confirmed', true)->first();
        return view('user.restricted-area', compact('user', 'currentReservation'));

    }

    public function orders($account)

    {

        $merchant = $this->getMerchant();


        $user = Auth::user();
        $currentReservation = $user->reservations()->whereDate('arrival', '<=', Carbon::today())->whereDate('departure', '>', Carbon::today())->where('confirmed', true)->first();

        $user = Auth::user();

        if($currentReservation) {
            $all_recipe_ids=[];
            $tray_ids=[];
            foreach ($currentReservation->trays as $tray) {
                $all_recipe_ids[] = $tray->recipes()->pluck('recipes.id')->toArray();
                $tray_ids[]=$tray->id;
            }
            $all_menus = [];
            foreach ($all_recipe_ids as $menu) {
                $all_menus[] = array_count_values($menu);
            }
            $slots = [
                '7:40',
                '8:00',
                '8:20',
                '8:40',
                '9:00',
                '9:20',
                '9:40',
                '10:00',
                '10:20',
                '10:40',
                '11:00',
            ];
            $busySlots = [];

            $busySlots = Order::withoutGlobalScopes()
            ->where('merchant_id',$merchant->id)
            ->where('day', Carbon::today()->addDays(1))
            ->pluck('slot')
            ->toArray();

            $availableSlots = array_diff($slots, $busySlots);
            $orders = $currentReservation->orders;

            return view('user.orders', compact('user', 'currentReservation', 'all_menus', 'availableSlots', 'orders', 'tray_ids'));
        }

        return redirect()->back()->with('error', 'no-reservation');
    }

    public function destroyTray($tray_id)
    {
        $tray = Tray::find($tray_id);
        $tray->recipes()->detach();
        $tray->delete();

        return redirect()->back();
    }

    public function reOrder($account, $order_id)
    {




        $order = Order::withoutGlobalScopes()->find($order_id);

        $newOrder = Order::create([
            'reservation_id' => $order->reservation_id,
            'total' => $order->total,
            'slot' => $order->slot,
            'room_service' => $order->room_service,
            'day' => Carbon::today()->addDays(1),
            'merchant_id' => $order->merchant_id
        ]);



        $attachableRows = $order->recipes->keyBy('id')->transform(function($recipe,$key){
            return ['quantity' => $recipe->pivot->quantity];
        });




        $newOrder
        ->recipes()
        ->attach(
            $attachableRows->toArray()
        );

        return redirect()->route('user.order-summary', ['order_id' => $newOrder->id,'account' => $account]);

    }




    // Pagamento dell'ordine
    public function pay($account,$reservation_id)
    {
        $user = Auth::user();
        $reservation = Reservation::find($reservation_id);

        if(!$reservation || $reservation->user_id != auth()->id()){
            return abort(403);
        }

        return view('user.pay', compact('user', 'reservation'));
    }

}
