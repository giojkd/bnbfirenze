<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cartalyst\Stripe\Exception\CardErrorException;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use App\Models\Reservation;

class PaymentController extends Controller
{
    private $publicKey;
    private $currency;

    public function __construct()
    {
        $this->publicKey = config('services.stripe.key');
        $this->currency = config('services.stripe.currency');
    }

    public function key()
    {
        return [ 'key' => $this->publicKey ];
    }

    public function stripeCallback(Request $request)
    {
        $paymentIntentId = $request->input('paymentIntentId');
        $paymentMethodId = $request->input('paymentMethodId');

        $items = $request->input('items');



        try
        {
            if($paymentMethodId != null)
            {
                $reservation = Reservation::find($items[0]['reservationId']);

                if(!$reservation || $reservation->user_id != auth()->id()){
                    return abort(403);
                }

                $price = $reservation->balance;

                // Create new PaymentIntent with a PaymentMethod ID from the client.
                $intent = Stripe::paymentIntents()->create([
                    "amount" => $price,
                    "currency" => $this->currency,
                    "payment_method" => $paymentMethodId,
                    "confirmation_method" => "manual",
                    "confirm" => true,
                    "metadata" => [
                        'product_id' => $reservation->id,
                        'product_name' => $reservation->code,
                        'user_email' => Auth::user()->email,
                    ],
                    // If a mobile client passes `useStripeSdk`, set `use_stripe_sdk=true`
                    // to take advantage of new authentication features in mobile SDKs
                    "use_stripe_sdk" => $request->input('useStripeSdk'),
                ]);
                // After create, if the PaymentIntent's status is succeeded, fulfill the order.
                // TODO:: qui potrebbe essere spostato il controllo che setta l'ordine a pagato e che in questo momento viene lanciato da JS
            }
            else if ($paymentIntentId != null)
            {
                $intent = Stripe::paymentIntents()->confirm($paymentIntentId);
                // After confirm, if the PaymentIntent's status is succeeded, fulfill the order.
                // TODO:: qui potrebbe essere spostato il controllo che setta l'ordine a pagato e che in questo momento viene lanciato da JS
            }

            return $this->generateResponse($intent);

        } catch (CardErrorException  $e) {
            return [
                'error' => $e->getMessage()
            ];
        }
    }

    private function generateResponse($intent)
    {
        switch($intent['status']) {
            case "requires_action":
            case "requires_source_action":
                // Card requires authentication
                return [
                    'requiresAction'=> true,
                    'paymentIntentId'=> $intent['id'],
                    'clientSecret'=> $intent['client_secret']
                ];
            case "requires_payment_method":
            case "requires_source":
                // Card was not properly authenticated, suggest a new payment method
                return [
                    error => "Your card was denied, please provide a new payment method"
                ];
            case "succeeded":
                // Payment is complete, authentication not required
                // To cancel the payment after capture you will need to issue a Refund (https://stripe.com/docs/api/refunds)
                // TODO:: qui potrebbe essere spostato il controllo che setta l'ordine a pagato e che in questo momento viene lanciato da JS
                return ['clientSecret' => $intent['client_secret']];
        }
    }

    public function handleWebhook()
    {
        return response(null, 200);
        $payload = @file_get_contents('php://input');
        $event = null;

        try
        {
            $event = \Stripe\Event::constructFrom(
                json_decode($payload, true)
            );
        }
        catch(\UnexpectedValueException $e)
        {
            return response(null, 400);
        }

        switch ($event->type) {
            case 'payment_intent.succeeded':
                $paymentIntent = $event->data->object; // contains a \Stripe\PaymentIntent
                // handlePaymentIntentSucceeded($paymentIntent);
                break;

            case 'payment_intent.payment_failed':
                $paymentIntent = $event->data->object; // contains a \Stripe\PaymentMethod
                // handlePaymentMethodAttached($paymentMethod);
                break;

            default:
                return response(null, 400);
                break;
        }

        return response(null, 200);
    }






    public function stripePaymentValidation($account,$reservation_id, $paymentId)
    {
        $reservation = Reservation::find($reservation_id);

        if(!$reservation || $reservation->user_id != auth()->id()){
            return abort(403);
        }

        $intent = Stripe::paymentIntents()->find($paymentId);

        if($intent['status'] == "succeeded"){
            $reservation->update([
                'paid' => true,
            ]);
        }

        return redirect('/pagamento-completato');
    }

    public function paymentCompleted()
    {
        return view('user.payment-completed');
    }
}
