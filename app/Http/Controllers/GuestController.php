<?php

namespace App\Http\Controllers;

use App\Models\Merchant;
use App\Traits\MerchantTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class GuestController extends Controller
{
    use MerchantTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $merchant = $this->getMerchant();


        if(Auth::user() && Auth::user()->isChef())
            return redirect()->route('chef');

        $data = [
            'account' => $merchant->domain_name
        ];



        return view('home',$data);
    }

    public function contactUs()
    {
        return view('contact-us');
    }

    public function privacyPolicy()
    {
        return view('privacy-policy');
    }

    // Scelta della lingua
    public function chooseLang($account,$lang)
    {
        session()->put('chooselanguage', $lang);
        return redirect()->back();
    }
    /*
    public function testAccount($account)
    {

        return $account;
    }
    */



}
