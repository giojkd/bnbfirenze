<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Menu;
use App\Models\Room;
use App\Models\Tray;
use App\Models\Order;
use App\Models\Recipe;
use App\Models\Category;
use App\Models\Reservation;
use App\Traits\MerchantTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class FrontendController extends Controller
{
    use MerchantTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function rooms()
    {
        $merchant = $this->getMerchant();
        $rooms = $merchant->rooms()->where('published', true)->get();
        return view('rooms', compact('rooms'));
    }

    public function room($account, $room_id)
    {

        $currentReservation = Auth::user()->reservations()->whereDate('arrival', '<=', Carbon::today())->whereDate('departure', '>', Carbon::today())->where('confirmed', true)->first();
        $room = Room::where('published', true)->where('id', $room_id)->first();
        if($room)
            return view('room', compact('room', 'currentReservation'));

        return redirect('/scopri-le-camere');
    }

    public function thankYou()
    {
        return view('thank-you');
    }

    public function menus()
    {
        $merchant = $this->getMerchant();
        $fixedMenu = $merchant->menus()->where('published', true)->where('price', '!=' ,null)->get();
        $customMenus = $merchant->menus()->where('published', true)->where('price', null)->orderBy('created_at', 'desc')->get();
        $currentReservation = Auth::user()->reservations()->whereDate('arrival', '<=', Carbon::today())->whereDate('departure', '>', Carbon::today())->where('confirmed', true)->first();

        if($currentReservation)
            return view('menus', compact('customMenus', 'currentReservation', 'fixedMenu'));

        return redirect()->back()->with('error', 'no-reservation');
    }

    public function externalMenu()
    {

        $merchant = $this->getMerchant();





        $fixedMenu = $merchant->menus()->where('published', true)->where('price', '!=' ,null)->get();
        $customMenus = $merchant->menus()->where('published', true)->where('price', null)->orderBy('created_at', 'desc')->get();



        $currentReservation = Reservation::create([
            'arrival' => Carbon::today(),
            'departure' => Carbon::today()->addDay(1),
            'adults' => 0,
            'children' => 0,
            'user_id' => Auth::user()->id,
            'room_id' => NULL,
            'gdpr' => true,
            'confirmed' => true,
            'merchant_id' => $merchant->id
        ]);

        return view('menus', compact('customMenus', 'currentReservation', 'fixedMenu'));

    }

    public function fixedMenus()
    {
        $menus = Menu::where('published', true)->get();
        return view('fixed-menus', compact('menus'));
    }

    public function menu(Request $request, $account,$menu_id,$name)
    {



        $menu = Menu::where('published', true)->where('id', $menu_id)->first();
        $order = Order::whereIn('reservation_id', Auth::user()->reservations->pluck('id'))->where('day', Carbon::today()->addDays(1))->first();



        if($menu && $order) {
            return redirect()->route('user.order-summary', $order->id);
        } else if($menu) {
            return view('menu', compact('menu'));
        } else {
            return redirect('/scopri-i-menu');
        }
    }

    public function newOrder(Request $request, $account, $reservation_id)
    {
        $merchant = $this->getMerchant();
        $reservation = Reservation::find($reservation_id);
        $trays = $reservation->trays;
        $totalCost = array_sum($reservation->trays()->pluck('trays.total')->toArray());
        if ($request->room_service !== NULL) {
            $order = Order::create([
                'reservation_id' => $reservation->id,
                'total' => $totalCost,
                'slot' => $request->slot,
                'room_service' => $request->room_service,
                'day' => Carbon::today()->addDays(1),
                'notes' => $request->notes ?: null,
                'merchant_id' => $merchant->id
            ]);
        }else{
            $order = Order::create([
                'reservation_id' => $reservation->id,
                'total' => $totalCost,
                'slot' => $request->slot,
                'room_service' => false,
                'day' => Carbon::today()->addDays(1),
                'notes' => $request->notes ?: null,
                'merchant_id' => $merchant->id
            ]);
        }

        $all_recipe_ids=[];

        foreach ($trays as $tray) {
            foreach ($tray->recipes()->pluck('recipes.id') as $recipe_id) {
                $all_recipe_ids[] = $recipe_id;
            }
        }

        foreach ($trays as $tray) {
            $tray->recipes()->detach();
            $tray->delete();
        }
        $all_recipe_ids = array_count_values($all_recipe_ids);
        foreach ($all_recipe_ids as $recipe_id => $quantity) {
            $order->recipes()->attach($recipe_id, ['quantity' => $quantity]);
        }

        return redirect()->route('user.order-summary', $order->id);
    }

    public function orderSummary($account,$order_id)
    {
        $order = Order::find($order_id)->load('recipes', 'reservation.user');
        $currentReservation = Auth::user()->reservations()->whereDate('arrival', '<=', Carbon::today())->whereDate('departure', '>', Carbon::today())->where('confirmed', true)->first();
        if($order && (Auth::user()->id == $order->reservation->user->id || Auth::user()->isAdmin()))
            return view('user.order-summary', compact('order', 'currentReservation'));

        return redirect()->back()->with('error', 'no-reservation');
    }

    public function chef()
    {
        if(Auth::user() && !Auth::user()->isChef()){
            return redirect()->route('home');
        }

        return view('chef');
    }



}
