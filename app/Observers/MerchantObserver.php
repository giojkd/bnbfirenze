<?php

namespace App\Observers;

use App\Models\Merchant;

class MerchantObserver
{
    /**
     * Handle the Merchant "created" event.
     *
     * @param  \App\Models\Merchant  $merchant
     * @return void
     */
    public function created(Merchant $merchant)
    {
        //

    }

    /**
     * Handle the Merchant "updated" event.
     *
     * @param  \App\Models\Merchant  $merchant
     * @return void
     */
    public function updated(Merchant $merchant)
    {
        //

    }

    /**
     * Handle the Merchant "deleted" event.
     *
     * @param  \App\Models\Merchant  $merchant
     * @return void
     */
    public function deleted(Merchant $merchant)
    {
        //
    }

    /**
     * Handle the Merchant "restored" event.
     *
     * @param  \App\Models\Merchant  $merchant
     * @return void
     */
    public function restored(Merchant $merchant)
    {
        //
    }

    /**
     * Handle the Merchant "force deleted" event.
     *
     * @param  \App\Models\Merchant  $merchant
     * @return void
     */
    public function forceDeleted(Merchant $merchant)
    {
        //
    }
}
