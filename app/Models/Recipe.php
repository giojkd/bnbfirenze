<?php

namespace App\Models;

use App\Scopes\MerchantScope;
use App\Traits\MerchantTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Recipe extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;
    use MerchantTrait;
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($recipe) {
            Translation::create([
                'translatable_type' => 'App\Models\Recipe',
                'translatable_id' => $recipe->id,
                'lang' => 'it',
                'title' => $recipe->name,
                'content' => $recipe->description,
            ]);
        });





        // static::updated(function ($recipe) {

        // });
    }

    protected static function booted()
    {
        static::addGlobalScope(new MerchantScope);
    }

    // Relazioni
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function menus()
    {
        return $this->belongsToMany(Menu::class);
    }

    public function trays()
    {
        return $this->belongsToMany(Tray::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class)->withPivot('quantity');
    }

    public function translations() {
        return $this->morphMany(Translation::class, 'translatable');
    }

    public function getTitleAttribute()
    {
        return optional($this->translations()->where('lang', config('app.locale'))->first())->title;
    }

    public function getContentAttribute()
    {
        return optional($this->translations()->where('lang', config('app.locale'))->first())->content;
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('gallery')
            ->useFallbackUrl('/img/recipe.jpg');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('cover')
              ->width(1400)
              ->sharpen(9);

        $this->addMediaConversion('thumb')
              ->width(400)
              ->sharpen(8);
    }
}
