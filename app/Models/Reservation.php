<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReservationConfirmed;
use App\Scopes\MerchantScope;
use App\Traits\MerchantTrait;

class Reservation extends Model
{
    use HasFactory;
    use MerchantTrait;
    protected $guarded = [];

    protected $dates = [
        'arrival',
        'departure'
    ];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($reservation) {
            $reservation->code = 'RES-' . now()->format('ymd') . '-' . rand(1000, 9999);
        });

        static::updating(function ($reservation) {
            if($reservation->getOriginal('confirmed') == false && $reservation->confirmed == true) {
                Mail::to(optional($reservation->user)->email ?: 'info@leylasclub.com')->send(new ReservationConfirmed($reservation));
            }
        });





    }

    protected static function booted()
    {
        static::addGlobalScope(new MerchantScope);
    }

    public function addBalance($total)
    {
        $this->balance = $this->balance + $total;
        $this->save();
    }

    public function getStartingbalanceAttribute()
    {
        return ($this->adults + $this->children) * 5;
    }

    public function getStartingstarsAttribute()
    {
        return (($this->adults) * 2) + $this->children ;
    }

    public function getDaterangeAttribute()
    {
        $range = [];

        if($this->arrival && $this->departure) {
            for ($i=0; $i < $this->arrival->diffInDays($this->departure) ; $i++) {
                $range[] = $this->arrival->addDays($i)->format('Y-m-d');
            }
        }

        return $range;
    }

    // Relazioni
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function room()
    {
        return $this->belongsTo(Room::class);
    }
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    public function trays()
    {
        return $this->hasMany(Tray::class);
    }
}
