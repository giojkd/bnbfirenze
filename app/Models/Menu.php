<?php

namespace App\Models;

use App\Scopes\MerchantScope;
use App\Traits\MerchantTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Menu extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;
    use MerchantTrait;
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($menu) {
            Translation::create([
                'translatable_type' => 'App\Models\Menu',
                'translatable_id' => $menu->id,
                'lang' => 'it',
                'title' => $menu->name,
                'content' => $menu->description,
            ]);
        });


        static::addGlobalScope(new MerchantScope);


        // static::updated(function ($menu) {

        // });
    }

    public function url()
    {
        return route('menu', [$this->id, \Str::slug($this->name)]);
    }

    // Relazioni
    public function recipes()
    {
        return $this->belongsToMany(Recipe::class);
    }

    public function trays()
    {
        return $this->hasMany(Tray::class);
    }

    public function translations() {
        return $this->morphMany(Translation::class, 'translatable');
    }

    public function getTitleAttribute()
    {
        return optional($this->translations()->where('lang', config('app.locale'))->first())->title;
    }

    public function getContentAttribute()
    {
        return optional($this->translations()->where('lang', config('app.locale'))->first())->content;
    }

    // Spatie Media Library
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('gallery')
             ->useFallbackUrl('/img/menu.jpg');;
    }
}
