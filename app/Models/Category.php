<?php

namespace App\Models;

use App\Scopes\MerchantScope;
use App\Traits\MerchantTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Category extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;
    use MerchantTrait;

    protected $guarded = [];


    protected static function boot()
    {
        parent::boot();

        static::created(function ($category) {
            Translation::create([
                'translatable_type' => 'App\Models\Category',
                'translatable_id' => $category->id,
                'lang' => 'it',
                'title' => $category->name,
            ]);
        });

        // static::updated(function ($category) {

        // });
    }

    protected static function booted(){ static::addGlobalScope(new MerchantScope); }


    public function getTitleAttribute()
    {
        return optional($this->translations()->where('lang', config('app.locale'))->first())->title;
    }

    // Relazioni
    public function recipes()
    {
        return $this->hasMany(Recipe::class);
    }

    public function translations() {
        return $this->morphMany(Translation::class, 'translatable');
    }

    // Spatie Media Library
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('picture')
             ->useFallbackUrl('/img/menu.jpg');;
    }



}
