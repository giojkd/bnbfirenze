<?php

namespace App\Models;

use App\Scopes\MerchantScope;
use App\Traits\MerchantTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Room extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;
    use MerchantTrait;
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($room) {
            Translation::create([
                'translatable_type' => 'App\Models\Room',
                'translatable_id' => $room->id,
                'lang' => 'it',
                'title' => $room->name,
                'content' => $room->description,
            ]);
        });





        // static::updated(function ($room) {

        // });
    }

    protected static function booted()
    {
        static::addGlobalScope(new MerchantScope);
    }

    public function url()
    {
        return route('room', [$this->id, \Str::slug($this->name)]);
    }

    // Anteprima
    public function preview($anteprima) {
        $anteprima = strip_tags($anteprima);
        $anteprima = substr($anteprima, 0 , 140);
        $anteprima = $anteprima . "...";
        return $anteprima;
    }

    public function getTitleAttribute()
    {
        return optional($this->translations()->where('lang', config('app.locale'))->first())->title;
    }

    public function getContentAttribute()
    {
        return optional($this->translations()->where('lang', config('app.locale'))->first())->content;
    }

    public function getPreview() {
        return $this->preview($this->content ?: $this->description);
    }

    // Relazioni
    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function translations() {
        return $this->morphMany(Translation::class, 'translatable');
    }

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('gallery')
            ->useFallbackUrl('/img/room.jpg');
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('cover')
              ->width(1400)
              ->sharpen(9);

        $this->addMediaConversion('thumb')
              ->width(400)
              ->sharpen(8);
    }
}
