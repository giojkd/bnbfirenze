<?php

namespace App\Models;

use App\Mail\OrderReadyMessage;
use App\Scopes\MerchantScope;
use App\Traits\MerchantTrait;
use Illuminate\Support\Facades\Mail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Order extends Model
{
    use HasFactory;
    use MerchantTrait;

    protected $guarded = [];

    protected $dates = ['day'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($order) {
            $order->code = (optional($order->reservation->room)->name ?: 'external' . $order->reservation->id) . '-' . $order->day->format('d-m-y');
        });

        static::updating(function ($order) {
            if($order->getOriginal('ready') == false && $order->ready == true) {
                Mail::to($order->reservation->user->email)->send(new OrderReadyMessage($order));
            }
        });

        static::saved(function ($order) {
            // $order->reservation->addBalance($order->total);
            $total = 0;
            foreach ($order->reservation->orders as $o) {
                $total = $total + $o->total;
            }
            $order->reservation->balance = $total;
            $order->reservation->save();
        });

        static::deleting(function ($order) {
            if($order->recipes) {
                $order->recipes()->detach();
            }
        });

        static::deleted(function ($order) {
            if($order->ready == false) {
                $order->reservation->addBalance(- $order->total);
            }
        });




    }

    protected static function booted()
    {
        static::addGlobalScope(new MerchantScope);
    }

    // Relazioni
    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }

    public function recipes()
    {
        return $this->belongsToMany(Recipe::class)->withPivot('quantity');
    }
}
