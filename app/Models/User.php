<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getTraysAttribute()
    {
        $trays = optional($this->reservations()->where('confirmed', true)->orderBy('created_at', 'desc')->first())->trays();
        return optional($trays)->count();
    }

    public function isAdmin()
    {
        return $this->role == 'admin';
    }

    public function isChef()
    {
        return $this->role == 'chef';
    }

    public function canImpersonate()
    {
        return $this->isAdmin();
    }

    public function canBeImpersonated()
    {
        return !$this->isAdmin();
    }



    // Relazioni


    public function merchants()
    {
        return $this->belongsToMany(Merchant::class)->withTimestamps();
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}
