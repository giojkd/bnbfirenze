<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Merchant extends Model
{

    use HasFactory;

    protected $guarded = [];

    public function translatable()
    {
        return $this->morphTo();
    }

    public function users(){
        return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function setNameAttribute($value){
        $this->attributes['domain_name'] = Str::slug($value);
        $this->attributes['name'] = $value;
    }

    public function rooms(){
        return $this->hasMany(Room::class);
    }


    public function menus()
    {
        return $this->hasMany(Menu::class);
    }

    public function orders(){
        return $this->hasMany(Order::class);
    }

}
