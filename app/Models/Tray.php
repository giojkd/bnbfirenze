<?php

namespace App\Models;

use App\Traits\MerchantTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tray extends Model
{
    use HasFactory;
    use MerchantTrait;
    protected $guarded = [];

    protected $dates = ['day'];

    // Relazioni
    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }

    public function menu()
    {
        return $this->belongsTo(Menu::class);
    }

    public function recipes()
    {
        return $this->belongsToMany(Recipe::class);
    }
}
