<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\FormController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PaymentController;


Route::group(['middleware' => 'merchant'],function(){
    Route::domain('{account}.'.env('APP_DOMAIN'))->group(function () {

        Route::get('/', [GuestController::class, 'index'])->name('home');
        Route::get('/contatti', [GuestController::class, 'contactUs'])->name('contact-us');
        Auth::routes();
        Route::get('/scopri-le-camere', [FrontendController::class, 'rooms'])->name('rooms');
        Route::get('/camera/{room_id}/{room_name?}', [FrontendController::class, 'room'])->name('room');
        Route::get('/scopri-i-menu/utente-esterno', [FrontendController::class, 'externalMenu'])->name('external-menus');
        Route::get('/i-miei-ordini', [UserController::class, 'orders'])->name('user.orders');
        Route::get('/menu/{menu_id}/{menu_name?}', [FrontendController::class, 'menu'])->name('menu');
        Route::get('/scopri-i-menu', [FrontendController::class, 'menus'])->name('menus');

        Route::get('/area-riservata', [UserController::class, 'restrictedArea'])->name('user.restricted-area');

        Route::get('/riepilogo-ordine/{order_id}', [FrontendController::class, 'orderSummary'])->name('user.order-summary');

        // Rotte per il pagamento
        Route::get('/paga/{reservation_id}', [UserController::class, 'pay'])->name('user.pay');
        Route::get('/acquista/{reservation_id}/stripe/{paymentId}/validate', [PaymentController::class, 'stripePaymentValidation'])->name('purchase.stripe.validate');
        Route::get('/stripe/key', [PaymentController::class, 'key'])->name('stripe.key');
        Route::post('/stripe', [PaymentController::class, 'stripeCallback'])->name('stripe.callback');
        Route::post('/stripe/payment-webhook', [PaymentController::class, 'handleWebhook'])->name('stripe.handle.webhook');
        Route::get('/pagamento-completato', [PaymentController::class, 'paymentCompleted'])->name('user.payment-completed');

        // Multilingua
        Route::get('/choose/language/{lang}', [GuestController::class, 'chooseLang'])->name('choose.lang');
        Route::get('finalizza-ordine/{reservation_id}', [FrontendController::class, 'newOrder'])->name('new-order');

        Route::get('/grazie', [FrontendController::class, 'thankYou'])->name('thank-you');

        Route::get('/chef', [FrontendController::class, 'chef'])->name('chef');

        Route::post('/riordina/{order_id}', [UserController::class, 'reOrder'])->name('re-order');
    });
});

Route::get('/privacy-policy', [GuestController::class, 'privacyPolicy'])->name('privacy-policy');
Route::post('/richiesta-info', [FormController::class, 'contactForm'])->name('contact-form');

Route::get('/menu-fissi', [FrontendController::class, 'fixedMenus'])->name('fixed-menus');
Route::get('/ordine-temporaneo', [FrontendController::class, 'temporaryOrder'])->name('temporary-order');

Route::delete('/delete/{tray_id}', [UserController::class, 'destroyTray'])->name('destroy-tray');

