// var script = document.createElement('script');
// script.onload = function () {
// startStripe();
// };
// script.src = 'https://js.stripe.com/v3/';

// document.head.appendChild(script); //or something of the likes

var startStripe = function() {
    // A reference to Stripe.js
    let csrfToken = document.head.querySelector('meta[name="csrf-token"]').content;
    let form = document.getElementById("payment-form");
    let reservationId = form.getAttribute('reservation-id');
    let publicKeyUrl = form.getAttribute('public-key-url');
    let callbackUrl = form.getAttribute('callback-url');

    var stripe;

    var orderData = {
        '_token': csrfToken,
        items: [{ reservationId: reservationId }]
    };

    // Disable the button until we have Stripe set up on the page
    document.querySelector("button").disabled = true;

    fetch(publicKeyUrl)
        .then(function(result) {
            return result.json();
        })
        .then(function(data) {
            return setupElements(data);
        })
        .then(function({ stripe, card, clientSecret }) {
            document.querySelector("button").disabled = false;

            form.addEventListener("submit", function(event) {
                event.preventDefault();
                pay(stripe, card, clientSecret);
            });
        });

    var setupElements = function(data) {
        stripe = Stripe(data.key);

        /* ------- Set up Stripe Elements to use in checkout form ------- */
        var elements = stripe.elements();
        var style = {
            base: {
                color: "#32325d",
                fontFamily: 'Arial, sans-serif',
                fontSmoothing: "antialiased",
                fontSize: "16px",
                "::placeholder": {
                    color: "#32325d"
                }
            },
            invalid: {
                fontFamily: 'Arial, sans-serif',
                color: "#fa755a",
                iconColor: "#fa755a"
            }
        };
        var card = elements.create("card", { style: style });
        // Stripe injects an iframe into the DOM
        card.mount("#card-element");

        return {
            stripe: stripe,
            card: card,
            clientSecret: data.clientSecret
        };
    };

    var handleAction = function(clientSecret) {
        stripe.handleCardAction(clientSecret).then(function(data) {
            if (data.error) {
                showError("Your card was not authenticated, please try again");
            } else if (data.paymentIntent.status === "requires_confirmation") {
                fetch(callbackUrl, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            '_token': csrfToken,
                            paymentIntentId: data.paymentIntent.id
                        })
                    }).then(function(result) {
                        return result.json();
                    })
                    .then(function(json) {
                        if (json.error) {
                            showError(json.error);
                        } else {
                            orderComplete(clientSecret);
                        }
                    });
            }
        });
    };

    /*
     * Collect card details and pays for the order
     */
    var pay = function(stripe, card) {
        changeLoadingState(true);

        stripe
            .createPaymentMethod("card", card)
            .then(function(result) {
                if (result.error) {
                    showError(result.error.message);
                } else {
                    orderData.paymentMethodId = result.paymentMethod.id;

                    return fetch(callbackUrl, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify(orderData)
                    });
                }
            })
            .then(function(result) {
                return result.json();
            })
            .then(function(paymentData) {
                if (paymentData.requiresAction) {
                    // Request authentication
                    handleAction(paymentData.clientSecret);
                } else if (paymentData.error) {
                    showError(paymentData.error);
                } else {
                    orderComplete(paymentData.clientSecret);
                }
            });
    };

    /* ------- Post-payment helpers ------- */

    /* Shows a success / error message when the payment is complete */
    var orderComplete = function(clientSecret) {
        stripe.retrievePaymentIntent(clientSecret).then(function(result) {
            var paymentIntent = result.paymentIntent;
            let paymentId = paymentIntent.id;

            window.location.replace("/acquista/" + reservationId + "/stripe/" + paymentId + "/validate");

            changeLoadingState(false);
        });
    };

    var showError = function(errorMsgText) {
        changeLoadingState(false);
        var errorMsg = document.querySelector(".sr-field-error");
        errorMsg.textContent = errorMsgText;
        setTimeout(function() {
            errorMsg.textContent = "";
        }, 4000);
    };

    // Show a spinner on payment submission
    var changeLoadingState = function(isLoading) {
        if (isLoading) {
            document.querySelector("button").disabled = true;
            document.querySelector("#spinner").classList.remove("hidden");
            document.querySelector("#button-text").classList.add("hidden");
        } else {
            document.querySelector("button").disabled = false;
            document.querySelector("#spinner").classList.add("hidden");
            document.querySelector("#button-text").classList.remove("hidden");
        }
    };
}

$(document).ready(function() {
    startStripe();
})