<div>
    <!-- Alert Profilo aggiornato -->
    @if($updateMessage)
    <div class="alert alert-success">
        {{ $updateMessage }}
        <button type="button" wire:click="$set('updateMessage', null)" >
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <!-- Form-submit -->
    <form role="form" class="w-100 mt-4" wire:submit.prevent="formUpdate" autocomplete="on" >
    @csrf
        <fieldset>
            
            <div class="form-group mb-4">
                <div>
                    <label for="company_name">{{ __('Ragione sociale') }}</label>
                    <input tabindex="1" type="text" class="@error('company_name') border-danger @enderror form-control" wire:model.lazy="company_name" id="company_name" name="company_name" placeholder="*{{ __('Ragione sociale') }}">
                </div>
                {{-- @error('company_name') <p class="text-danger p-1 mb-0">{{ __('Campo obbligatorio (16 caratteri).') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <label for="fiscal_code">{{ __('Codice Fiscale') }}</label>
                    <input tabindex="2" type="text" maxlength="16" class="@error('fiscal_code') border-danger @enderror form-control" wire:model.lazy="fiscal_code" id="fiscal_code" name="fiscal_code" placeholder="*{{ __('Codice Fiscale') }}" >
                </div>
                {{-- @error('fiscal_code') <p class="text-danger p-1 mb-0">{{ __('Campo obbligatorio (16 caratteri).') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <label for="vat_number">{{ __('P.IVA') }}</label>
                    <input tabindex="3" type="text" maxlength="16" class="@error('vat_number') border-danger @enderror form-control" wire:model.lazy="vat_number" id="vat_number" name="vat_number" placeholder="*{{ __('P.IVA') }}">
                </div>
                {{-- @error('vat_number') <p class="text-danger p-1 mb-0">{{ __('Campo obbligatorio (16 caratteri).') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <label for="pec_sdi">{{ __('PEC o SDI') }}</label>
                    <input tabindex="4" type="text" maxlength="16" class="@error('pec_sdi') border-danger @enderror form-control" wire:model.lazy="pec_sdi" id="pec_sdi" name="pec_sdi" placeholder="*{{ __('PEC o SDI') }}">
                </div>
                {{-- @error('pec_sdi') <p class="text-danger p-1 mb-0">{{ __('Campo obbligatorio (16 caratteri).') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <label for="address">{{ __('Indirizzo') }}</label>
                    <input tabindex="5" type="text" class="@error('address') border-danger @enderror form-control" wire:model.lazy="address" id="address" name="address" placeholder="*{{ __('Indirizzo') }}" >
                </div>
                {{-- @error('address') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror --}}
            </div>
            
            <div class="form-group mb-4">
                <div>
                    <label for="cap">{{ __('Cap') }}</label>
                    <input tabindex="6" type="number" class="@error('cap') border-danger @enderror form-control" wire:model.lazy="cap" id="cap" name="cap" placeholder="*{{ __('Cap') }}" >
                </div>
                {{-- @error('cap') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <label for="city">{{ __('Città') }}</label>
                    <input tabindex="7" type="text" class="@error('city') border-danger @enderror form-control" wire:model.lazy="city" id="city" name="city" placeholder="*{{ __('Città') }}" >
                </div>
                {{-- @error('city') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <label for="province">{{ __('Provincia') }}</label>
                    <input tabindex="8" type="text" class="@error('province') border-danger @enderror form-control" wire:model.lazy="province" id="province" name="province" placeholder="*{{ __('Inserire la siglia della Provincia (es FI)') }}" >
                </div>
                {{-- @error('region') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <label for="country">{{ __('Nazione') }}</label>
                    <input tabindex="9" type="text" class="@error('country') border-danger @enderror form-control" wire:model.lazy="country" id="country" name="country" placeholder="{{ __('Nazione') }}" >
                </div>
                {{-- @error('country') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror --}}
            </div>
    
            <div class="form-group">
                <div class="text-center">
                    <button tabindex="11" type="submit" class="btn btn-lg btn-dark btn-block btn-rounded mb-4 py-3 text-uppercase"><i class="far fa-edit mr-2"></i>{{ __('Aggiorna profilo') }}</button>
                </div>
            </div>
    
        </fieldset>
    </form>
</div>
