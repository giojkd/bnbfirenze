<div>
    <!-- Alert Profilo completato -->
    @if($successMessage)
    <div class="alert alert-success">
        {{ $successMessage }}
        <button type="button" wire:click="$set('successMessage', null)" >
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    @endif

    <!-- Form-submit -->
    <form id="contact-form" role="form" class="w-100 mt-4" wire:submit="submitForm" autocomplete="on" >
    @csrf
        <fieldset>
            
            <div class="form-group mb-4">
                <div>
                    <input tabindex="1" type="text" class="@error('company_name') border-danger @enderror form-control" wire:model.lazy="company_name" id="company_name" name="company_name" placeholder="*{{ __('Ragione sociale') }}">
                </div>
                {{-- @error('company_name') <p class="text-danger p-1 mb-0">{{ __('Campo obbligatorio (16 caratteri).') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <input tabindex="2" type="text" maxlength="16" class="@error('fiscal_code') border-danger @enderror form-control" wire:model.lazy="fiscal_code" id="fiscal_code" name="fiscal_code" placeholder="*{{ __('Codice Fiscale') }}" >
                </div>
                {{-- @error('fiscal_code') <p class="text-danger p-1 mb-0">{{ __('Campo obbligatorio (16 caratteri).') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <input tabindex="3" type="text" maxlength="16" class="@error('vat_number') border-danger @enderror form-control" wire:model.lazy="vat_number" id="vat_number" name="vat_number" placeholder="*{{ __('P.IVA') }}">
                </div>
                {{-- @error('vat_number') <p class="text-danger p-1 mb-0">{{ __('Campo obbligatorio (16 caratteri).') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <input tabindex="4" type="text" maxlength="16" class="@error('pec_sdi') border-danger @enderror form-control" wire:model.lazy="pec_sdi" id="pec_sdi" name="pec_sdi" placeholder="*{{ __('PEC o SDI') }}">
                </div>
                {{-- @error('pec_sdi') <p class="text-danger p-1 mb-0">{{ __('Campo obbligatorio (16 caratteri).') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <input tabindex="5" type="text" class="@error('address') border-danger @enderror form-control" wire:model.lazy="address" id="address" name="address" placeholder="*{{ __('Indirizzo') }}" >
                </div>
                {{-- @error('address') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror --}}
            </div>
            
            <div class="form-group mb-4">
                <div>
                    <input tabindex="6" type="number" class="@error('cap') border-danger @enderror form-control" wire:model.lazy="cap" id="cap" name="cap" placeholder="*{{ __('Cap') }}" >
                </div>
                {{-- @error('cap') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <input tabindex="7" type="text" class="@error('city') border-danger @enderror form-control" wire:model.lazy="city" id="city" name="city" placeholder="*{{ __('Città') }}" >
                </div>
                {{-- @error('city') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <input tabindex="8" type="text" class="@error('province') border-danger @enderror form-control" wire:model.lazy="province" id="province" name="province" placeholder="*{{ __('Inserire la siglia della Provincia (es FI)') }}" >
                </div>
                {{-- @error('region') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror --}}
            </div>

            <div class="form-group mb-4">
                <div>
                    <input tabindex="9" type="text" class="@error('country') border-danger @enderror form-control" wire:model.lazy="country" id="country" name="country" placeholder="*{{ __('Nazione') }}" >
                </div>
                {{-- @error('country') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror --}}
            </div>
    
            {{-- <div class="form-group mb-4">
                <div>
                    <label for="birthdate" class="bold">{{ __('Data di nascita') }}</label>
                    <input tabindex="6" type="date" class="@error('birthdate') border-danger @enderror form-control" wire:model.lazy="birthdate" id="birthdate" name="birthdate" placeholder="*{{ __('Data di nascita') }}" >
                </div>
                @error('birthdate') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror
            </div> --}}
    
            <div class="form-group mb-4 mb-0">
                <div>
                    <label for="gdpr">
                        <input tabindex="10" type="checkbox" value="1" id="gdpr" name="gdpr" style="height: auto !important; width: auto !important;" required>
                        {{ __('Accetto termini e condizioni') }} ({{ __('GDPR') }}) - <a href="{{ route('privacy-policy') }}" target="_blank" class="text-dark"><strong>{{ __('Leggi qui') }}</strong></a>
                    </label>
                </div>
            </div>
    
            <div class="form-group">
                <div class="text-center">
                    <button tabindex="11" type="submit" class="btn btn-lg btn-dark btn-block btn-rounded mb-4 py-3 text-uppercase">{{ __('Completa profilo') }}</button>
                </div>
            </div>
    
        </fieldset>
    </form>
</div>
