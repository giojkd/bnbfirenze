<div>
    <!-- Form -->
    <form id="contact-form" role="form"  wire:submit.prevent="submitForm" autocomplete="on">
    @csrf
        <fieldset>
    
            @if (session()->has('message'))
                <div class="alert alert-danger">
                   <i class="fa fa-exclamation-circle mr-2"></i>
                    {{ session('message') }}
                </div>
            @endif

            <div class="form-group row mb-4">
                <div class="col-6">
                    <label for="arrival" class="bold">{{ __('Check-in') }}</label>
                    <input tabindex="1" type="date" class="@error('arrival') border-danger @enderror form-control" wire:model.lazy="arrival" id="arrival" name="arrival" placeholder="*{{ __('Check in') }}" required>
                </div>
                <div class="col-6">
                    <label for="departure" class="bold">{{ __('Check-out') }}</label>
                    <input tabindex="2" type="date" class="@error('departure') border-danger @enderror form-control" wire:model.lazy="departure" id="departure" name="departure" placeholder="*{{ __('Check out') }}" required>
                </div>
                <div class="col-12">
                    @error('arrival') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror
                    @error('departure') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror
                </div>
            </div>
    
            <div class="form-group row mb-4">
                <div class="col-6">
                    <label for="adults" class="bold">{{ __('Adulti') }}</label>
                    <input tabindex="3" type="number" max="4" min="0" class="@error('adults') border-danger @enderror form-control" wire:model.lazy="adults" id="adults" name="adults" placeholder="*{{ __('Adulti') }}" required>
                </div>
                <div class="col-6">
                    <label for="children" class="bold">{{ __('Bambini < 12 anni') }}</label>
                    <input tabindex="4" type="number" max="4" min="0" class="@error('children') border-danger @enderror form-control" wire:model.lazy="children" id="children" name="children" placeholder="*{{ __('Bambini') }}">
                </div>
                <div class="col-12">
                    @error('adults') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror
                    @error('children') <p class="text-danger p-1 mb-0">{{ __('Questo campo è obbligatorio.') }}</p> @enderror
                </div>
            </div>
            
            <input type="hidden" wire:model="room_id">

            <input type="hidden" wire:model="user_id">
    
            <div class="form-group mb-4 mb-0">
                <div>
                    <label for="gdpr">
                        <input tabindex="6" type="checkbox" value="1" wire:model.lazy="gdpr" id="gdpr" name="gdpr" required>
                        {{ __('Accetto termini e condizioni') }} ({{ __('GDPR') }}) - <a href="{{ route('privacy-policy') }}" target="_blank" class="text-dark"><strong>{{ __('Leggi qui') }}</strong></a>
                    </label>
                </div>
            </div>
    
            <div class="form-group">
                <div class="text-center">
                    <button tabindex="7" type="submit" class="btn btn-lg btn-dark btn-block rounded-0 mb-4 py-2 text-uppercase">{{ __('Invia Richiesta') }}</button>
                </div>
            </div>
    
        </fieldset>
    </form>
</div>

@push('scripts')
    <script>
        // Selezionabili solo date future per arrivo e partenza
        $(function(){
            var dtToday = new Date();
            
            var month = dtToday.getMonth() + 1;
            var day = dtToday.getDate();
            var year = dtToday.getFullYear();
            if(month < 10)
                month = '0' + month.toString();
            if(day < 10)
                day = '0' + day.toString();
            
            var maxDate = year + '-' + month + '-' + day;

            $('#arrival').attr('min', maxDate);
            $('#departure').attr('min', maxDate);
        });
    </script>
@endpush