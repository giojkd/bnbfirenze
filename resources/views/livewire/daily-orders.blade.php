<div class="row justify-content-center py-5">

    <!-- Ordini da gestire -->
    <div class="col-12 col-lg-6 mb-4">
        <div class="h-100 bg-light p-3">
            <h4 class="bold text-center mb-4">{{ __('Ordini da gestire') }}</h4>
            <hr>
            @forelse ($dailyOrders as $dailyOrder)
            <div class="col-12 bold my-4">
                <!-- Riepilogo -->
                <p class="lead bold text-center mb-0">{{ optional($dailyOrder->reservation->room)->name ?: 'external' . $dailyOrder->reservation->id }}</p>

                <p class="lead bold text-center mb-0">Giorno: {{ $dailyOrder->day->format('d/m') }}</p>
                <p class="lead bold text-center">{{ $dailyOrder->slot }}</p>
                <table class="table table-hover my-4">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('Nome') }}</th>
                            <th scope="col">{{ __('Quantità') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($dailyOrder->recipes->all() as $recipe)
                        <tr>
                            <td>{{ $recipe->title ?: $recipe->name }}</td>
                            <td>{{ $recipe->pivot->quantity }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <p class="font-weight-normal text-center">{{ $dailyOrder->room_service ? __('Servizio in camera') : __('Ordine in sala') }}</p>
                @if ($dailyOrder->notes)
                <p class="bold">{{ __('Note aggiuntive') }} : </p>
                <p class="font-weight-normal">{{ $dailyOrder->notes }}</p>
                @endif
                <button class="btn btn-lg btn-success btn-block rounded-0 bold text-uppercase mb-5" wire:click.prevent='readyOrder({{$dailyOrder->id}})'>Pronto</button>
                @if(!$loop->last)<hr>@endif
            </div>
            @empty
            <div class="col-12 text-center">
                <p class="lead">{{ __('Nessun ordine da gestire') }}</p>
            </div>
            @endforelse
        </div>

    </div>

    <!-- Separatore -->
    <div class="col-12 d-lg-none">
        <hr class="my-5 border-secondary">
    </div>

    <!-- Ordini pronti -->
    <div class="col-12 col-lg-6 mb-4">
        <div class="h-100 bg-lightgreen p-3">
            <h4 class="bold text-center mb-4">{{ __('Ordini completati') }}</h4>
            <hr>
            @forelse ($readyOrders as $readyOrder)
            <div class="col-12 bold my-4">
                <!-- Riepilogo -->
                <p class="lead bold text-center mb-0">{{ optional($readyOrder->reservation->room)->name ?: 'external' . $readyOrder->reservation->id }}</p>
                <p class="lead bold text-center mb-0">Giorno: {{ $readyOrder->day->format('d/m') }}</p>
                <p class="lead bold text-center">{{ $readyOrder->slot }}</p>
                <p class="text-center"><a href="#" class="inherit" data-toggle="collapse" data-target="#order-collapsed{{ $readyOrder->id }}"><i class="far fa-info-circle mr-2"></i>{{ __('Mostra ordine') }}</a></p>
                <div id="order-collapsed{{ $readyOrder->id }}" class="collapse">
                <table class=" table table-hover my-4">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('Nome') }}</th>
                            <th scope="col">{{ __('Quantità') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($readyOrder->recipes->all() as $recipe)
                        <tr>
                            <td>{{ $recipe->title ?: $recipe->name }}</td>
                            <td>{{ $recipe->pivot->quantity }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <p class="font-weight-normal text-center">{{ $readyOrder->room_service ? __('Servizio in camera') : __('Ordine in sala') }}</p>
                @if ($readyOrder->notes)
                <p class="bold">{{ __('Note aggiuntive') }} : <span class="font-weight-normal">{{ $readyOrder->notes }}</span></p>
                @endif
                </div>
                @if(!$loop->last)<hr>@endif
            </div>
            @empty
            <div class="col-12 text-center">
                <p class="lead">{{ __('Nessun ordine completato') }}</p>
            </div>
            @endforelse
        </div>
    </div>
</div>
