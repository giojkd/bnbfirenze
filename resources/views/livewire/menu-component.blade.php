<div>

    <!-- Budget iniziale menu alla carta -->
    @auth
    @if ($menu->price == NULL && $reservation->room)
    <div class="row flex-column position-sticky balance-summary shadow">
        <div class="col-12">
            <p class="lead text-uppercase font-weight-bold text-center my-3">{{ __('Budget giornaliero') }}</p>
            <div class="d-flex justify-content-around pb-3">
                <div class="d-flex w-50 justify-content-center align-items-center flex-column border-right border-dark">
                    <div class="display-5">{{ $stars }}</div>
                    <p class="lead mb-2">{{ __('piatti stella') }}</p>
                    <div><i class="fas fa-star fa-2x text-warning"></i></div>
                </div>
                <div class="d-flex w-50 justify-content-center align-items-center flex-column"> 
                    <div class="display-5">€ {{ $balance <= 0 ? 0 - $balance : $balance }}</div>
                    <p class="lead {{ $balance <= 0 ? 'text-success' : 'text-danger' }} mb-2">{{ $balance <= 0 ? 'bonus' : __('costo dell\'ordine') }}</p>
                    <div><i class="fas fa-money-bill fa-2x text-success"></i></div>
                </div>
            </div>
        </div>
    </div>
    @elseif($menu->price == NULL)
    <div class="row flex-column position-sticky balance-summary shadow">
        <div class="col-12">
            <p class="lead text-uppercase font-weight-bold text-center my-3">{{ __('Costo') }}</p>
            <div class="d-flex justify-content-around pb-3">
                <div class="d-flex w-50 justify-content-center align-items-center flex-column"> 
                    <div class="display-5">€ {{ $balance <= 0 ? 0 - $balance : $balance }}</div>
                    <p class="lead text-danger mb-2">{{ __('costo dell\'ordine') }}</p>
                    <div><i class="fas fa-money-bill fa-2x text-success"></i></div>
                </div>
            </div>
        </div>
    </div>
    @else
    <!-- Nessun budget disponibile -->
    @endif
    @endauth
    
    <!-- Ricette presenti nel menu -->
    <div class="row py-5">

        <div class="col-12">

            @if ($menu->price !== NULL)
            <!-- Menu fissi -->
            <div class="row py-4">
                @foreach ($types as $type)
                <div class="col-12 col-md-6">
                    <h3><strong>{{$type}}</strong></h3>
                    @if($menu->recipes->where('type', $type)->count() > 1)
                    <p>{{ __('Scegli tra le varianti') }}</p>
                    @endif
                    @foreach ($menu->recipes->where('type', $type) as $recipe)
                    <div class="radio-button">
                        <input type="radio" id="{{ $recipe->id }}" name="type-{{Str::slug($type)}}" wire:click="addRecipeFixedMenu({{ $recipe->id }})">
                        <label for="{{$recipe->id}}"><strong>{{ $recipe->title ?: $recipe->name }}</strong></label>
                    </div>
                    @endforeach
                    <hr>
                </div>
                @endforeach
            </div>
            @else

            <!-- Menu alla carta -->
            <div class="row py-4">
                @foreach ($categories as $category)
                <div class="col-12 col-md-6 mb-4">
                    <h3 class="d-flex align-items-center bold mb-4">
                        <img src="{{ $category->getFirstMediaUrl('picture') }}" class="mr-2" height="40">
                        {{$category->title ?: $category->name}}
                    </h3>
                    @foreach ($category->recipes as $recipe)
                    @if ($recipe->published)
                    <div class="row">
                        <div class="col-8 col-md-9">
                            <strong><i class="mr-2 {{ $recipe->starred ? 'fas fa-star text-warning' : 'far fa-star text-secondary' }}"></i>
                            {{ $recipe->title ?: $recipe->name }}</strong>
                            <p class="my-0 pl-4">€ {{ $recipe->price }}</p>
                        </div>
                        <div class="col-4 col-md-3 d-flex justify-content-between align-items-center">
                            <button type="button" data-recipe-minus="{{ $recipe->id }}" class="btn counter-button minus d-flex justify-content-center align-items-center"  wire:click="removeRecipe({{ $recipe->id }})">
                                <i class="text-center float-right fas fa-minus"></i>
                            </button>
                            <span id="counter-{{ $recipe->id }}" wire:ignore class="bold">0</span>
                            <button type="button" data-recipe-plus="{{ $recipe->id }}" class="btn counter-button plus d-flex justify-content-center align-items-center"  wire:click="addRecipe({{ $recipe->id }})">
                                <i class="text-center float-right fas fa-plus"></i>
                            </button>
                        </div>
                    </div>
                    @if ($recipe->description)
                    <div class="ml-4">{!! $recipe->content ?: $recipe->description !!}</div>
                    @endif
                    @if(!$loop->last)<hr>@endif
                    @endif
                    @endforeach
                </div>
                @endforeach
            </div>

            @endif
            <!-- Messaggio errore -->
            <div>
                @if (session()->has('message'))
                    <div class="alert alert-danger text-center">
                        {{ session('message') }}
                    </div>
                @endif
            </div>

            <!-- Creazione del vassoio -->
            <button type="button" wire:click="newTray({{ $menu->id }})" class="fixed-bottom-smart btn btn-lg btn-block btn-dark text-uppercase bold rounded-0 py-3">{!! __('Aggiungi all\'ordine') !!}</button>
                        
        </div>
        
    </div>
    
    @push('scripts')

        <!-- Tasti quantità piatto -->
        <script>
            const plus = document.querySelectorAll('[data-recipe-plus]');
            const minus = document.querySelectorAll('[data-recipe-minus]');
            
            plus.forEach(el => {
                el.addEventListener('click', () => {
                    const recipe_id = el.getAttribute('data-recipe-plus')
                    const counter = document.querySelector(`#counter-${recipe_id}`)
                    counter.innerHTML = Number(counter.innerHTML) + 1
                })
            })

            minus.forEach(el => {
                el.addEventListener('click', () => {
                    const recipe_id = el.getAttribute('data-recipe-minus')
                    const counter = document.querySelector(`#counter-${recipe_id}`)
                    if (counter.innerHTML > 0) counter.innerHTML = Number(counter.innerHTML) - 1
                })
            })
        </script>

    @endpush

</div>