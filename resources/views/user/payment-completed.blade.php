<x-layouts.app
    title="Pagamento completato"
    description="Hai saltato il conto delle colazioni per la tua camera nel B&B La Marmora 39 a Firenze"
>

@push('styles')

@endpush

<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-40">

        <div class="col-12 text-white text-center"> 

            <h1 class="bold text-shadowed">{{ __('Pagamento completato') }}</h1>

        </div>

    </header>

</div>

<div class="container pt-4">

    <section class="row justify-content-center pt-5">

        <div class="col-12 text-center mb-5">

            <figure>
                <img src="/img/logo-bnb-la-marmora-39.svg" alt="B&B La Marmora, 39 (Firenze)" class="img-fluid" width="250">
            </figure>

        </div>

    </section>

    <section class="row justify-content-center pb-5">

        <div class="col-12 col-lg-6 text-center">

            <p>{{ __('Congratulazioni, hai saldato il conto delle colazioni per il tuo soggiorno presso il B&B La Marmora, 39 (Firenze)') }}</p>

        </div>

    </section>

</div>

<footer class="container-fluid">

    <hr class="border-secondary">

    <div class="container">

        <div class="row py-4">

            <div class="col-12 text-center">

                <p>
                    {{ __('B&B La Marmora 39, di AQSKINS Srl - Via Alfonso La Marmora, 39 50121 Firenze - P.I.: 06878700480 - ') }}
                    <a href="{{ route('privacy-policy') }}" target="_blank" class="inherit">
                        {{ __('Privacy Policy')}}
                    </a>
                </p>

            </div>

        </div>

    </div>

</footer>

@push('scripts')

@endpush

</x-layouts.app>