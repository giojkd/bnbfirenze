<x-layouts.app
    title="Riepilogo ordini"
    description="Questo è il riepilogo dei tuoi ordini"
>

@push('styles')

    <style>
        .radio-button {
            font-size: 1.1rem;
        }
    </style>

@endpush

<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-30">

        <div class="col-12 text-white text-center">

            <h1 class="bold text-shadowed">{{ __('Riepilogo ordini') }}</h1>

        </div>

    </header>

</div>

@if($currentReservation && $currentReservation->trays->count() > 0)
<div class="container">

    <section class="row justify-content-center py-5">



        <div class="col-12 col-md-6 text-center">

            <h2 class="bold mb-4">{{ __('Ordine da inviare') }}</h2>

        </div>

        <div class="col-12 col-md-6 text-center mb-3">
            <a href="{{ route('menus') }}" class="btn btn-success text-uppercase bold rounded-0 py-1 px-3"><strong><p class="m-0">{{ __('Aggiungi menu') }}</p></strong></a>
        </div>

        <!-- Ordine attuale da confermare -->
        @foreach ($all_menus as $menu)
        <div class="col-12 col-md-6 col-lg-4 bold my-4">
            <form method="post" action="{{ route('destroy-tray', $tray_ids[$loop->index]) }}">
                @method('delete')
                @csrf
                <p class="lead text-center text-uppercase">{{ optional(App\Models\Tray::find($tray_ids[$loop->index])->menu)->name }}<button type="submit" class="btn btn-danger ml-4">&times;</button></p>
            </form>
            <!-- Riepilogo -->
            <table class="table mt-4">
                <thead>
                    <tr>
                        <th scope="col">{{ __('Nome') }}</th>
                        <th scope="col">{{ __('Quantità') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($menu as $recipe_id => $quantity)
                    <tr>
                        <td>{{ App\Models\Recipe::find($recipe_id)->name }}</td>
                        <td>{{ $quantity }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @endforeach

        <div class="col-12 text-center">
            <p class="h4 bold mb-4">{{ 'TOT. € ' . array_sum($currentReservation->trays()->pluck('total')->toArray()) }}</p>
            <hr class="my-5">
        </div>

        <form action="{{ route('new-order', $currentReservation ) }}" method="get" class="container mb-5">
        @csrf
            <div class="row bg-light py-5">
                <div class="col-12 mb-4">
                    <p class="lead bold text-center mb-3">{{ __('Seleziona un orario') }}:</p>
                </div>
                @foreach ($availableSlots as $slot)
                <div class="col-6 col-md-2 text-center my-3">
                    <div class="radio-button badge badge-dark px-3 pb-1 pt-2">
                        <input type="radio" id="{{$slot}}" name="slot" value="{{$slot}}" required>
                        <label for="{{$slot}}" class="ml-1"><strong>{{$slot}}</strong></label>
                    </div>
                </div>
                @endforeach

                @if ($currentReservation->room)
                <div class="col-12 text-center my-4">
                    <input type="checkbox" id="room_service" name="room_service" value="1">
                    <label for="room_service"><strong>{{ __('Colazione in camera?') }}</strong></label>
                </div>
                @endif
                <div class="col-12 text-center my-4">
                    <div> <label for="notes">{{ __('Note aggiuntive') }} :</label> </div>
                    <textarea name="notes" id="notes" cols="50" rows="4"></textarea>
                </div>
                <div class="col-12 text-center mt-3">
                    <button type="submit" class="btn btn-lg btn-dark text-uppercase bold rounded-0 py-3 px-4">{{ __('Invia ordine allo Chef') }}</button>
                </div>
            </div>
        </form>

    </section>

</div>
@endif

<div class="container">

    <!-- Ordini inviati -->
    <section class="row py-5">

        <div class="col-12 text-center">

            <h2 class="bold mb-4">{{ __('Ordini inviati') }}</h2>

        </div>

        @forelse($orders as $order)
        <div class="col-12 col-md-6 col-xl-4 mb-5">

            <div class="card rounded-0 p-3">
                <h4 class="bold mb-2">Ordine del {{ optional($order->day)->format('d.m.Y') }}</h4>

                <p class="mb-2">Tot. € {{ $order->total }}</p>

                <p class="mb-2">Ore {{ $order->slot }} ({{ $order->room_service ? 'in camera' : 'in sala' }})</p>

                <p class="small mb-0">
                    <a href="#" data-toggle="collapse" data-target="#order-info-{{$order->id}}" class="text-secondary"><i class="far fa-info-circle mr-1"></i>{{ __('Mostra dettagli') }}</a>
                </p>

                <div id="order-info-{{$order->id}}" class="collapse mt-3">
                    @foreach($order->recipes as $recipe)
                    <p>{{ $recipe->pivot->quantity }}x <strong>{{ $recipe->title ?: $recipe->name }}</strong></p>
                    <p class="">€ {{ $recipe->price * $recipe->pivot->quantity }}</p>
                    @if(!$loop->last)<hr>@endif
                    @endforeach
                </div>

                @if ($order->day < Carbon\Carbon::today())
                    <form action="{{ route('re-order', $order->id) }}" method="post" class="mt-3">
                        @csrf
                        <button type="submit" class="btn btn-sm rounded-0 btn-outline-warning bold">{{ __('Riordina') }}</button>
                    </form>
                @endif
            </div>
        </div>
        @empty
        <div class="col-12 text-center mb-4">
            <p>{{ __('Non hai ancora inviato nessun ordine') }}. <a href="{{ route('menus') }}" class="bold">{{ __('Vai al menu') }}<i class="far fa-sign-in ml-2"></i></a></p>
        </div>
        @endforelse

        <div class="col-12 text-center">
            <a href="{{ route('home') }}" class="btn btn-lg btn-dark text-uppercase bold rounded-0 py-3 px-5">{{ __('Torna alla home') }}</a>
        </div>

    </section>

</div>

@push('scripts')

@endpush

</x-layouts.app>
