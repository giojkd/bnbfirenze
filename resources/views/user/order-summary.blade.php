<x-layouts.app
    title="Riepilogo ordine"
    description="Questo è il riepilogo del tuo ordine"
>

@push('styles')

@endpush

<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-30">

        <div class="col-12 text-white text-center"> 

            <h1 class="bold text-shadowed">{{ __('Riepilogo ordine') }}<br> {{ ($order->day)->format('d.m.Y') }}</h1>

        </div>

    </header>

</div>

<div class="container my-5">

    <div class="row justify-content-center py-5">

        <div class="col-12 col-md-8 col-xl-6 text-center">
            
            @foreach($order->recipes as $recipe)
            <p>{{ $recipe->pivot->quantity }}x <strong>{{ $recipe->title ?: $recipe->name }}</strong></p>
            <p class="">€ {{ $recipe->pivot->quantity * $recipe->price }}</p>
            <hr>
            @endforeach
            @if ($order->notes)
            <p class="bold">{{ __('Note aggiuntive') }} : </p>
            <p> {{ $order->notes }} </p>
            @endif
            <p class="bold">Ore {{ $order->slot }} ({{ $order->room_service ? 'in camera' : 'in sala' }})</p>
            <p class="bold">Tot. € {{ $order->total }}</p>
            
        </div>
        @if ($currentReservation->room)
        <div class="col-12 text-center mt-5">
            <a href="{{ route('home') }}" class="btn btn-lg btn-dark text-uppercase bold rounded-0 py-3 px-5">{{ __('Torna alla home') }}</a>
        </div>
        @else
        <div class="col-12 text-center mt-5">
            <a href="{{ route('user.restricted-area') }}" class="btn btn-lg btn-dark text-uppercase bold rounded-0 py-3 px-5">{{ __('Vai al pagamento') }}</a>
        </div>
        @endif

    </div>

</div>

@push('scripts')
    
@endpush

</x-layouts.app>