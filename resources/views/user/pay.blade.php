<x-layouts.app
    title="Pagamento online"
    description="Paga con carta il saldo della tua camera nel B&B La Marmora 39 a Firenze"
>

@push('styles')
    <link href="{{ mix('css/stripe.css') }}" rel="stylesheet">
    <style>
        @media screen and (max-width: 600px) {
            .bg-mobile-light {
                background-color: #f8f9fa;
            }
        }
    </style>
@endpush

<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-40">

        <div class="col-12 text-white text-center"> 

            <h1 class="bold text-shadowed">{{ __('Paga online') }}</h1>

        </div>

    </header>

</div>

<div class="container stripe-container">

    <section class="row justify-content-center py-5">

        <div class="col-12 col-lg-6 mb-5">
            <p class="h2 bold mb-4">{{ __('Dati fatturazione') }}:</p>
            @if(Auth::user()->profile)
        
            <table class="table table-striped table-bordered table-sm">
                <tr>
                    <th>{{ __('Email') }}:</th>
                    <td>{{Auth::user()->email}}</td>
                </tr>
                <tr>
                    <th>{{ __('Ragione sociale') }}:</th>
                    <td>{{Auth::user()->profile->company_name}}</td>
                </tr>
                <tr>
                    <th>{{ __('P. IVA') }}:</th>
                    <td>{{Auth::user()->profile->vat_number}}</td>
                </tr>
                <tr>
                    <th>{{ __('PEC o ') }}:</th>
                    <td>{{Auth::user()->profile->pec_sdi}}</td>
                </tr>
                <tr>
                    <th>{{ __('Codice Fiscale') }}:</th>
                    <td>{{Auth::user()->profile->fiscal_code}}</td>
                </tr>
                <tr>
                    <th>{{ __('Indirizzo') }}:</th>
                    <td>{{Auth::user()->profile->address}}</td>
                </tr>
                <tr>
                    <th>{{ __('Cap') }}:</th>
                    <td>{{Auth::user()->profile->cap}}</td>
                </tr>
                <tr>
                    <th>{{ __('Città') }}:</th>
                    <td>{{Auth::user()->profile->city}}</td>
                </tr>
                <tr>
                    <th>{{ __('Provincia') }}:</th>
                    <td>{{Auth::user()->profile->province}}</td>
                </tr>
                <tr>
                    <th>{{ __('Nazione') }}:</th>
                    <td>{{Auth::user()->profile->country}}</td>
                </tr>
                {{-- <tr>
                    <th>{{ __('Data di nascita') }}:</th>
                    <td>{{optional(Auth::user()->profile->birthdate)->format('d-m-Y')}}</td>
                </tr> --}}
            </table>
            @else
            <div class="mb-4">
                <div class="py-3">
                    <span class="h5 fw-bold bold mb-2">{{ __('Desideri la fattura?') }}</span>
                    <br class="d-md-none">
            
                    <span class="mx-3">
                        <label for="yes" class="form-check-label">Si</label>
                    </span>
                    <span class="mx-3">
                        <input type="radio" id="yes" class="form-check-input" name="check" value="1" style="padding: 0px; height: auto; width: auto;">
                    </span>
                    <span class="mx-3">
                        <label for="no" class="form-check-label">No</label>
                    </span>
                    <span class="mx-3">    
                        <input type="radio" id="no" class="form-check-input" name="check" value="0" style="padding: 0px; height: auto; width: auto;">
                    </span>     
                </div>
                <div id="form-profile" class="d-none">
                    <livewire:profile-form />
                </div>
            </div>
            @endif
        </div>

        <div class="col-12 col-lg-6 bg-mobile-light py-5 py-sm-0">

            @if($reservation->paid == true)
            <!-- Già pagato -->
            <p class="h2 bold text-center mb-4">{{ __('Nulla da saldare') }}</p>
            <div class="bg-lightgreen text-center p-3 mb-5">
                <p class="mb-0">{{ __('Conto delle colazioni già saldato') }}<i class="far fa-gift ml-2"></i></p>
            </div>
            <p class="text-center">
                <a href="{{ route('home') }}" class="btn btn-dark px-4 py-2">{{ __('Torna alla home') }}</a>
            </p>
            @else
            <!-- Form pagamento Stripe -->
            <p class="h2 bold text-center mb-4">Tot. € {{ $reservation->balance }}</p>
            <div class="d-flex w-100 justify-content-center mb-4">
                <x-stripe-form
                    publicKeyUrl="{{route('stripe.key')}}"
                    callbackUrl="{{route('stripe.callback')}}"
                    reservationId="{{$reservation->id}}"
                />
            </div>
            @endif

        </div>

    </section>

    <section class="row justify-content-center pb-5">

        <div class="col-12 text-center mb-5">

            <figure>
                <img src="/img/logo-bnb-la-marmora-39.svg" alt="B&B La Marmora, 39 (Firenze)" class="img-fluid" width="250">
            </figure>

        </div>

    </section>

</div>

<footer class="container-fluid">

    <hr class="border-secondary">

    <div class="container">

        <div class="row py-4">

            <div class="col-12 text-center">

                <p>
                    {{ __('B&B La Marmora 39, di AQSKINS Srl - Via Alfonso La Marmora, 39 50121 Firenze - P.I.: 06878700480 - ') }}
                    <a href="{{ route('privacy-policy') }}" target="_blank" class="inherit">
                        {{ __('Privacy Policy')}}
                    </a>
                </p>

            </div>

        </div>

    </div>

</footer>

@push('scripts')
    <script src="{{ mix('js/stripe.js') }}"></script>
    <script src="https://js.stripe.com/v3/"></script>

    {{-- toggle profile --}}
    <script>
        let collapse = document.querySelector('#form-profile')
        let yes = document.querySelector('#yes')
        let no = document.querySelector('#no')

        yes.addEventListener('click', () => {
            collapse.classList.remove('d-none');
        })
        no.addEventListener('click', () => {
            collapse.classList.add('d-none');
        })

    </script>
@endpush

</x-layouts.app>