<x-layouts.app
    title="Area riservata"
    description="Qui troverai profilo utente, prenotazioni e ordini"
>

@push('styles')
    <style>
        .img-room{
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
            border-radius: 100%;
            height: 100px;
            width: 100px;
        }
    </style>
@endpush

<div class="container-fluid bg-warning offset-navbar">

    <div class="container container-large">

        <header class="row align-items-center vh-30 py-5">

            <div class="col-12 text-center text-white">

                <h1 class="display-5 mb-4">{{ __('Area riservata') }}</h1>

            </div>

        </header>

    </div>

</div>

<div class="container-fluid bg-light">
    <div class="row">
        <div class="col-12 p-3">
            @if($currentReservation && $currentReservation->paid == false)
                <!-- Pagamento del soggiorno -->
                @if($currentReservation->balance > 0 && Carbon\Carbon::today() == $currentReservation->departure)
                    <div class="bg-alert text-center py-4">
                        <p class="text-dark m-0">{{ __('Vai al pagamento online') }}:</p>
                        <div class="text-dark information bold p-2 mb-2 py-1">€ {{$currentReservation->balance}}</div>
                        <a href="{{ route('user.pay', $currentReservation->id) }}" class="btn btn-warning">{{ __('Vai al pagamento') }}<i class="far fa-sign-in-alt ml-2"></i></a>
                        <p class="mt-3 mb-0 bold bg-alert">{{ __('Oppure salda tutto in Reception') }}</p>
                    </div>
                @endif
                <!-- Pagamento della colazione da esterno -->
                @if($currentReservation && !$currentReservation->room && $currentReservation->balance > 0)
                    <div class="bg-alert text-center py-4">
                        <p class="text-dark m-0">{{ __('Saldo ordinazione') }}:</p>
                        <div class="text-dark information bold p-2 mb-2 py-1">€ {{$currentReservation->balance}}</div>
                        <a href="{{ route('user.pay', $currentReservation->id) }}" class="btn btn-warning">{{ __('Paga la colazione') }}<i class="far fa-sign-in-alt ml-2"></i></a>
                        <p class="mt-3 mb-0 bold bg-alert">{{ __('Oppure salda tutto in Reception') }}</p>
                    </div>
                @endif
            @elseif($currentReservation && $currentReservation->paid == true)
                <div class="bg-alert text-center py-4">
                    <p class="text-dark m-0">{{ __('Conto già saldato') }}<i class="far fa-gift ml-2"></i></p>
                </div>
            @else
                
            @endif
        </div>
    </div>
</div>

<div class="container-fluid bg-light">

    <div class="row justify-content-center">

        <!-- Budget colazione -->
        <div class="col-12 col-lg-4 p-4 order-lg-3">
            <div class="h-100 bg-white p-4">
                @if($currentReservation && $currentReservation->room)
                <p class="lead text-uppercase font-weight-bold text-center px-4 pt-4">{{ __('Budget giornaliero') }}</p>
                <div class="d-flex py-3 flex-column">
                    <div class="d-flex justify-content-around breakfast p-3">
                        <div class="d-flex w-50 justify-content-center align-items-center flex-column border-right border-dark">
                            <div class="display-5">{{$currentReservation->startingStars}}</div>
                            <p class="lead my-3">{{ __('piatti stella') }}</p>
                            <div><i class="fas fa-star fa-2x text-warning"></i></div>
                        </div>
                        <div class="d-flex w-50 justify-content-center align-items-center flex-column"> 
                            <div class="display-5">€ {{$currentReservation->startingBalance}}</div>
                            <p class="lead my-3">{{ __('bonus') }}</p>
                            <div><i class="fas fa-money-bill fa-2x text-success"></i></div>
                        </div>
                    </div>
                    <p class="mt-5">
                        {!! __('B&B La Marmora 39 regala ad ogni camera un <strong>budget omaggio</strong> da spendere giornalmente sul menu alla carta.') !!}
                    </p>
                    <p>
                        {{ __('Nello specifico') }}:
                    </p>
                    <p>
                        {{ __('Ogni adulto può scegliere dal menu alla carta 2 piatti gratis contrassegnati da una stella più 1 bonus di €5 da spendere su piatti aggiuntivi.') }}
                    </p>
                    <p>
                        {{ __('I bambini minori di 12 anni dispongono di 1 solo piatto stella più 1 bonus di €5.') }}
                    </p>
                    <p>
                        {{ __('Il budget si rigenera ogni giorno alle 13:00, quindi, nel caso non venga speso, andrà perso.') }}
                    </p>
                    <a href="{{ route('menus') }}" class="text-light mt-3 text-decoration-none btn-dark text-center p-4 text-uppercase font-weight-bold"><strong><p class="m-0">{{ __('vai al menu colazione') }}</p></strong></a>
                </div>
                @elseif($currentReservation && !$currentReservation->room)
                <p class="lead text-uppercase font-weight-bold text-center px-4">{{ __('Vai ai menu per ordinare') }}:</p>
                <a href="{{ route('menus') }}" class="text-decoration-none btn btn-block btn-dark p-4 text-uppercase bold mt-3"><strong><p class="m-0">{{ __('I nostri menu') }}</p></strong></a>
                @else
                <p class="lead text-uppercase font-weight-bold text-center px-4">{{ __('sei un ospite del b&b') }}?</p>
                <hr>
                <p>
                    {{ __('Per ordinare la colazione, associa la tua camera inserendo i dati della tua prenotazione e attendi l\'email di conferma da parte dell’amministratore. In questo modo potrai accedere a sconti esclusivi sul menu.') }}
                </p>
                <a href="{{ route('rooms') }}" class="text-decoration-none btn btn-block btn-dark p-4 text-uppercase bold mt-3"><strong><p class="m-0">{{ __('Associa la tua camera') }}</p></strong></a>
                
                <hr class="my-4">

                <p class="bold text-center" style="margin-top: -36px;"><span class="bg-white px-1">{{ __('oppure') }}</span></p>

                <p class="lead text-uppercase font-weight-bold text-center px-4">{{ __('Sei un cliente esterno') }}?</p>
                <a href="{{ route('external-menus') }}" class="text-decoration-none btn btn-block btn-dark p-4 text-uppercase bold mt-3"><strong><p class="m-0">{{ __('Scopri i menu') }}</p></strong></a>
                @endif
            </div>
        </div>

        
        <!-- Info prenotazione -->
        @if($currentReservation)
        <div class="col-12 col-lg-4 p-4 text-center order-lg-2">

            <div class="h-100 bg-white p-4">
                @if($currentReservation->room)
                    <div class="col-12 mb-3 d-flex justify-content-center">
                        <div class="img-room" style="background-image:url('{{$currentReservation->room->getFirstMediaUrl('gallery', 'thumb') }}');"></div>
                    </div>
                    <h4 class="font-weight-bold text-uppercase mb-4 mt-2 py-1">{{$currentReservation->room->title ?: $currentReservation->room->name}}</h4>
                    <p class="m-0">{{ __('Codice prenotazione') }}:</p>
                    <div class="information bold p-2 mb-2 py-1">{{$currentReservation->code}}</div>
                    <p class="m-0">{{ __('Check-in') }}:</p>
                    <div class="information bold p-2 mb-2 py-1">{{optional($currentReservation->arrival)->format('d-m-Y')}}</div>
                    <p class="m-0">{{ __('Check-out') }}:</p>
                    <div class="information bold p-2 mb-2 py-1">{{optional($currentReservation->departure)->format('d-m-Y')}}</div>
                    <p class="m-0">{{ __('Adulti') }}:</p>
                    <div class="information bold p-2 mb-2 py-1">{{$currentReservation->adults}}</div>
                    @if ($currentReservation->children)
                        <p class="m-0">{{ __('Bambini') }}:</p>
                        <div class="information bold p-2 mb-2 py-1">{{$currentReservation->children}}</div>            
                    @endif
                @endif

                <a href="{{ route('user.orders') }}" class="btn btn-dark btn-block rounded-0 p-4 text-uppercase bold mt-3">{{ __('I miei ordini') }}</a>
            </div>
        </div>
        @endif

        

        <!-- Info utente -->
        <div class="col-12 col-lg-4 p-4 order-lg-1">
            <div class="h-100 bg-white p-4">
                <div class="text-center mb-4">
                    <div class="rounded-circle mx-auto" style="height: 100px; width: 100px; background-color:rgb(231, 142, 157);">
                        <i class="fas fa-user-circle text-primary" style="font-size: 6.8rem;"></i>
                    </div>

                    <h4 class="font-weight-bold text-uppercase mb-4 mt-2 py-1">{{$user->name . ' ' . $user->surname}}</h4>
                    <p class="m-0">{{ __('Indirizzo e-mail') }}:</p>
                    <div class="information bold p-2 mb-2 py-1">{{ $user->email }}</div>
                    <p class="m-0">{{ __('Numero di telefono') }}:</p>
                    <div class="information bold p-2 mb-2 py-1">{{ $user->phone ?: '-' }}</div>
                </div>

                <hr>

                <span class="small mb-0">
                    <i class="fal fa-info-circle"></i>
                    <a href="#" data-toggle="collapse" data-target="#profile-info" class="text-secondary">{{ $user->profile ? '' : __('Completa profilo') }}</a>
                </span>
                @if ($user->profile)
                    <span id="update">{{__('Mostra profilo')}}
                        <i class="far fa-edit ml-2"></i>
                    </span>
                    <div id="form" class="d-none">
                        <livewire:profile-update />
                    </div>
                @endif

                <div id="profile-info" class="mt-3 collapse">
                    @if($user->profile)
                    {{-- <p class="mb-0 font-italic">{{ __('Data di nascita') }}:</p>
                    <div class="mb-2 information bold p-2">{{optional($user->profile->birthdate)->format('d-m-Y')}}</div> --}}
                    {{-- <p class="mb-0 font-italic">{{ __('Ragione sociale') }}:</p>
                    <div class="mb-2 information bold p-2"> {{$user->profile->company_name}}</div>
                    <p class="mb-0 font-italic">{{ __('Codice Fiscale') }}:</p>
                    <div class="mb-2 information bold p-2"> {{$user->profile->fiscal_code}}</div>
                    <p class="mb-0 font-italic">{{ __('P. IVA') }}:</p>
                    <div class="mb-2 information bold p-2"> {{$user->profile->vat_number}}</div>
                    <p class="mb-0 font-italic">{{ __('PEC o SDI') }}:</p>
                    <div class="mb-2 information bold p-2"> {{$user->profile->pec_sdi}}</div>
                    <p class="mb-0 font-italic">{{ __('Indirizzo') }}:</p>
                    <div class="mb-2 information bold p-2"> {{$user->profile->address}}</div>
                    <p class="mb-0 font-italic">{{ __('Cap') }}:</p>
                    <div class="mb-2 information bold p-2"> {{$user->profile->cap}}</div>
                    <p class="mb-0 font-italic">{{ __('Città') }}:</p>
                    <div class="mb-2 information bold p-2"> {{$user->profile->city}}</div>
                    <p class="mb-0 font-italic">{{ __('Provincia') }}:</p>
                    <div class="mb-2 information bold p-2"> {{$user->profile->province}}</div>
                    <p class="mb-0 font-italic">{{ __('Nazione') }}:</p>
                    <div class="mb-2 information bold p-2"> {{$user->profile->country}}</div> --}}
                    
                    @else
                    <p>{{ __('Completa il tuo profilo per avere la possibilità di pagare online') }}:</p>
                    <livewire:profile-form />
                    @endif
                </div>
            </div>
        </div>

    </div>

</div>

@push('scripts')
    <script>
        let update = document.querySelector('#update')
        let form = document.querySelector('#form')
        update.addEventListener('click', () => {
            form.classList.toggle('d-none')
        })
    </script>
@endpush

</x-layouts.app>