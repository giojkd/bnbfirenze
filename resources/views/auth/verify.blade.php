<x-layouts.app
    title="Reimposta password"
    description="Questa è la descrizione per google"
>

<!-- Above the fold -->
<div class="container-fluid bg-warning offset-navbar">

    <div class="container container-large">

        <header class="row align-items-center vh-40 py-5">

            <div class="col-12 text-center text-white">

                <h1 class="display-5 mb-4">{{ __('Modifica password') }}</h1>

            </div>

        </header>

    </div>

</div>

<div class="container">
    <div class="row py-5 justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Verifica il tuo indirizzo EMail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un link di verifica è stato inviato al tuo indirizzo Email') }}
                        </div>
                    @endif

                    {{ __('Prima di procedere, controlla di aver ricevuto il link di verifica via email') }}
                    {{ __('Se non hai ricevuto l\'email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('clicca qui per inviarla di nuovo') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</x-layouts.app>
