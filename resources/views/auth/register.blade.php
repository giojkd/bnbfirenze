<x-layouts.app
    title="Registrati"
    description="Questa è la descrizione per google"
>

<!-- Above the fold -->
<div class="container-fluid bg-warning offset-navbar">

    <div class="container container-large">

        <header class="row align-items-center vh-40 py-5">

            <div class="col-12 text-center text-white">

                <h1 class="display-5 mb-4">{{ __('Registrati') }}</h1>

            </div>

        </header>

    </div>

</div>

<div class="container py-4 py-md-5">

    <div class="row justify-content-center py-5">

        <div class="col-12 col-md-8 col-xl-6">

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <div class="form-group">

                <label for="name" class="text-secondary">*{{ __('Nome') }}</label>
                <input id="name" type="text" class="form-control form-control-lg bg-lightgray border-0 rounded-0 @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">

                <label for="surname" class="text-secondary">*{{ __('Cognome') }}</label>
                <input id="surname" type="text" class="form-control form-control-lg bg-lightgray border-0 rounded-0 @error('surname') is-invalid @enderror" name="surname" value="{{ old('surname') }}" required autocomplete="surname" autofocus>

                @error('surname')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">

                <label for="email" class="text-secondary">*{{ __('E-Mail') }}</label>
                <input id="email" type="email" class="form-control form-control-lg bg-lightgray border-0 rounded-0 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">

                <label for="phone" class="text-secondary">{{ __('Telefono') }}</label>
                <input id="phone" type="phone" class="form-control form-control-lg bg-lightgray border-0 rounded-0 @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" autocomplete="phone">

                @error('phone')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">
                <label for="password" class="text-secondary">*{{ __('Password') }}</label>
                <input id="password" type="password" class="form-control form-control-lg bg-lightgray border-0 rounded-0 @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="form-group">
                <label for="password-confirm" class="text-secondary">*{{ __('Conferma Password') }}</label>
                <input id="password-confirm" type="password" class="form-control form-control-lg bg-lightgray border-0 rounded-0" name="password_confirmation" required autocomplete="new-password">
            </div>

            <div class="form-check my-3">
                <input class="form-check-input" type="checkbox" name="gdpr" id="gdpr" {{ old('gdpr') ? 'checked' : '' }} required>

                <label class="form-check-label" for="gdpr">
                    *{{ __('Accetto termini e condizioni') }} - <a href="{{ route('privacy-policy') }}" target="_blank" class="inherit bold">{{ __('Leggi qui') }}</a>
                </label>
            </div>

            <div class="form-group mb-4">
                <button type="submit" class="btn py-3 btn-dark btn-block text-uppercase rounded-0">
                    {{ __('Registrati') }}
                </button>
            </div>

            <p>{{ __('Hai già un account?') }} <a href="{{ route('login') }}" class="inherit bold">{{ __('Accedi') }}</a></p>

        </form>

    </div>

</div>

</x-layouts.app>
