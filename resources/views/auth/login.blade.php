<x-layouts.app
    title="Login"
    description="Questa è la descrizione per google"
>

<!-- Above the fold -->
<div class="container-fluid bg-warning offset-navbar">

    <div class="container container-large">

        <header class="row align-items-center vh-40 py-5">

            <div class="col-12 text-center text-white">

                <h1 class="display-5 mb-4">{{ __('Accedi') }}</h1>

            </div>

        </header>

    </div>

</div>

<div class="container py-4 py-md-5">

    <div class="row justify-content-center py-5">

        <div class="col-12 col-md-8 col-xl-6">

            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div class="form-group">
                    <label for="email" class="text-secondary">{{ __('E-Mail') }}</label>
                    <input id="email" type="email" class="form-control form-control-lg bg-lightgray border-0 rounded-0 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password" class="text-secondary">{{ __('Password') }}</label>
                    <input id="password" type="password" class="form-control form-control-lg bg-lightgray border-0 rounded-0 @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="form-group row">
                    <div class="col-12 col-md-6 order-md-2 text-right mb-2">
                        @if (Route::has('password.request'))
                        <a href="{{ route('password.request') }}">
                            {{ __('Password dimenticata') }}?
                        </a>
                        @endif
                    </div>
                    <div class="col-12 col-md-6 order-md-1  mb-2">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('Ricordami') }}
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn py-3 btn-outline-dark btn-block text-uppercase rounded-0">
                        {{ __('Accedi') }}
                    </button>
                </div>

                <hr class="my-4">

                <p class="bold text-center" style="margin-top: -36px;"><span class="bg-white px-1">{{ __('oppure') }}</span></p>

                <div class="form-group">
                    <a href="{{ route('register') }}" class="btn py-3 btn-dark btn-block text-uppercase rounded-0">
                        {{ __('Registrati') }}
                    </a>
                </div>

            </form>

        </div>

    </div>

</div>

</x-layouts.app>
