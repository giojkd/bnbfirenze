<x-layouts.app
    title="Leyla's Club - Chef"
    description="Tutti gli ordini in preparazione per la giornata di oggi"
>

@push('styles')

@endpush

<!-- Above the fold -->
<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-40">

        <div class="col-12 text-white text-center"> 

            <h1 class="bold text-shadowed">{{ __('Ordini del giorno') }}</h1>

        </div>

    </header>

</div>

<!-- Contenuto -->
<div class="container-fluid">

    <div class="container">
        <livewire:daily-orders />  
    </div>
</div>

@push('scripts')
    
@endpush

</x-layouts.app>