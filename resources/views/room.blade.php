<x-layouts.app
    title="{{ $room->name }}"
    description="Questa è la descrizione per google"
>

@push('styles')
    <style>

    </style>
@endpush

<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-40">

        <div class="col-12 text-white text-center">

            <h1 class="bold text-shadowed">{{ $room->title ?: $room->name }}</h1>

        </div>

    </header>

</div>

<div class="container-fluid">
    <div class="row p-3">
        <div class="col-12 bg-alert text-center">
            @if ($currentReservation && $currentReservation->room)
            <a href="{{ route('user.restricted-area') }}" class="text-decoration-none bg-alert bold"><p class="p-3 mt-2">{{__('Il tuo account risulta già associato a una camera, verifica nella tua area riservata ')}}<i class="far fa-sign-in-alt ml-2"></i></p></a>
            @endif
        </div>
    </div>
</div>

<div class="container pt-5">
    <div class="row justify-content-center pb-5">
        <div class="col-12 col-md-6">
            <h4 class="mb-3 text-uppercase">{{ __('associa la tua camera') }}</h4>
            <livewire:reservation-form />
        </div>
        <div class="col-12 col-md-6">
            <figure>
                <img src="{{ $room->getFirstMediaUrl('gallery', 'cover') }}" alt="{{ $room->title ?: $room->name }}" width="600" class="img-fluid">
            </figure>
        </div>
        <div class="col-12 mt-5">
            <div>{!! $room->content ?: $room->description !!}</div>
        </div>
    </div>
</div>

@push('scripts')

@endpush

</x-layouts.app>
