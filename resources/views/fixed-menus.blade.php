<x-layouts.app
    title="I nostri menu"
    description="Questa è la descrizione per google"
>

@push('styles')

@endpush

<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-30">

        <div class="col-12 text-white text-center"> 

            <h1 class="bold text-shadowed">{{ __('Menu fissi') }}</h1>

        </div>

    </header>

</div>

<div class="container pt-4">

    <div class="row justify-content-center py-5">

        @foreach ($menus->where('price', '!==', NULL) as $menu)
        <div class="col-12 col-md-6 col-lg-4 mb-5">
            <div class="card h-100 border-0 rounded-0 shadow">
                <div class="card-body">
                    <h3 class="d-flex align-items-center bold mb-4">
                        <img src="{{ $menu->getFirstMediaUrl('gallery') }}" class="mr-2" width="60">
                        {{ $menu->title ?: $menu->name }}
                    </h3>
                    @if ($menu->description)
                    <div class="p-3">{!! $menu->content ?: $menu->description !!}</div>
                    @else
                    <p class="bold p-3">{{ __('Scegli tra tutti i prodotti presenti nel nostro menu') }}</p>
                    @endif
                    @if ($menu->price)
                    <div class=" px-3 py-2 mt-auto bold text-success">€ {{$menu->price}}</div>
                    @endif
                </div>
                <a href="{{ $menu->url() }}" class="btn btn-block btn-dark rounded-0 border-0 text-uppercase bold mt-auto py-4" style="background-color: {{ $menu->color }}">{{ __('Scegli e personalizza') }}</a>
            </div>
        </div>
        @endforeach

    </div>

</div>

@push('scripts')
    
@endpush

</x-layouts.app>