<x-layouts.app
    title="{{ $menu->name }}"
    description="Questa è la descrizione per google"
>

@push('styles')
<style>
    @media screen and (max-width: 1079px) and (orientation: landscape) {
        .position-sticky {
            position: static !important;
        }
    }
</style>
@endpush

<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-30">

        <div class="col-12 text-white text-center"> 

            <h1 class="bold text-shadowed">{{ $menu->title ?: $menu->name }}</h1>

        </div>

    </header>

</div>

<div class="container-md">

    <livewire:menu-component :menu="$menu" />

</div>

@push('scripts')

@endpush

</x-layouts.app>