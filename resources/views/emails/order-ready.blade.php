@component('mail::message')

<center>
<img src="https://leylasclub.com/img/logo-black.svg" alt="Leyla's Club" width="50" style="margin-bottom: 20px;">
</center>

<h3 style="text-align: center">{{ __('La tua ordinazione è pronta') }}!</h3>

<hr>

<p>{{ optional($order->day)->format('d.m.Y') }}</p>

@if($order->room_service)
<p>{{ __('Il tuo ordine è stato preprato ed è in arrivo in') }} {{ optional($order->reservation->room)->name ?: 'reception' }}</p>
@else
<p>{{ __('Lo Chef ha preparato il tuo ordine delle ore') }} {{ $order->slot }}, {!! __('ti aspettiamo in sala.<br> Buona colazione!') !!} :)</p>
@endif

@if ($order->reservation->room)
<p>{!! __('Saldo dell\'ordine di oggi') !!}: €{{ $order->total }} ({{ __('aggiunto al conto del tuo soggiorno') }})</p>
<p>{{ __('Conto totale del soggiorno') }}: €{{ $order->reservation->balance }}</p>
@else
<p>{!! __('Saldo dell\'ordine di oggi') !!}: €{{ $order->total }}</p>
@endif

@endcomponent