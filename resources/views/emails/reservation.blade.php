@component('mail::message')

<h3 style="text-align: center">{{ __('Cod. Prenotazione') }} {{ $reservation->code }}</h3>

<hr>

<p>{{ $reservation->user ? $reservation->user->name . ' ' . $reservation->user->surname : 'Utente sconosciuto' }}</p>

<p>{{ optional($reservation->room)->name }}</p>

<p>{{ __('Check-in') }}: <span style="font-weight: bold">{{ optional($reservation->arrival)->format('d.m.Y') }}</span> - {{ __('Check-out') }}: <span style="font-weight: bold">{{ optional($reservation->departure)->format('d.m.Y') }}</span></p>

<a href="{{ config('app.url') }}/admin/resources/reservations/{{$reservation->id}}/edit" style="font-weight: bold;">{{ __('Gestisci prenotazione') }}</a>

@endcomponent