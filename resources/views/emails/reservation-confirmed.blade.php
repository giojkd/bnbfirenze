@component('mail::message')

<center>
<img src="https://leylasclub.com/img/logo-black.svg" alt="Leyla's Club" width="50" style="margin-bottom: 20px;">
</center>

<h3 style="text-align: center">{{ __('La camera è stata associata alla tua prenotazione') }}</h3>

<hr>

<p>Cod. : <strong style="font-weight: bold;">{{ $reservation->code }}</strong></p>

<p>{{ $reservation->user ? $reservation->user->name . ' ' . $reservation->user->surname : 'Utente sconosciuto' }}</p>

<p>{{ optional($reservation->room)->name }}</p>

<p>{{ __('Check-in') }}: <span style="font-weight: bold">{{ optional($reservation->arrival)->format('d.m.Y') }}</span> - {{ __('Check-out') }}: <span style="font-weight: bold">{{ optional($reservation->departure)->format('d.m.Y') }}</span></p>

@if($reservation->user)
<a href="https://leylasclub.com/area-riservata" style="font-weight: bold;">{{ __('Visita la tua area riservata') }}</a>
@else
<a href="https://leylasclub.com" style="font-weight: bold;">{{ __('Apri sito') }}</a>
@endif

@endcomponent