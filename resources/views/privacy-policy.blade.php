<x-layouts.app
    title="Privacy Policy"
    description="Termini e condizioni del sito leylasclub.com - B&B La Marmora, 39 (Firenze)"
>

@push('styles')

@endpush

<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-30">

        <div class="col-12 text-white text-center"> 

            <h1 class="bold text-shadowed">{{ __('Privacy Policy')}}</h1>

        </div>

    </header>

</div>

<div class="container pt-4">

    <section class="row justify-content-center py-5">

        <div class="col-12">

            <a href="https://www.iubenda.com/privacy-policy/20649713" class="iubenda-white no-brand iubenda-embed iub-body-embed" title="Privacy Policy">{{ __('Privacy Policy')}}</a>

            <a href="https://www.iubenda.com/privacy-policy/20649713/cookie-policy" class="iubenda-white no-brand iubenda-embed iub-body-embed" title="Cookie Policy">{{ __('Cookie Policy') }}</a>

        </div>

    </section>

</div>

<footer class="container-fluid">

    <hr class="border-secondary">

    <div class="container">

        <div class="row py-4">

            <div class="col-12 text-center">

                <p>
                    {{ __('B&B La Marmora 39, di AQSKINS Srl - Via Alfonso La Marmora, 39 50121 Firenze - P.I.: 06878700480 - ') }}
                    <a href="{{ route('privacy-policy') }}" target="_blank" class="inherit">
                        {{ __('Privacy Policy')}}
                    </a>
                </p>

            </div>

        </div>

    </div>

</footer>

@push('scripts')
    <!-- Iubenda -->
    <script type="text/javascript">(function (w,d) {var loader = function () {var s = d.createElement("script"), tag = d.getElementsByTagName("script")[0]; s.src="https://cdn.iubenda.com/iubenda.js"; tag.parentNode.insertBefore(s,tag);}; if(w.addEventListener){w.addEventListener("load", loader, false);}else if(w.attachEvent){w.attachEvent("onload", loader);}else{w.onload = loader;}})(window, document);</script>
@endpush

</x-layouts.app>