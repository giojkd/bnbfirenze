<form id="payment-form" public-key-url="{{$publicKeyUrl}}" callback-url="{{$callbackUrl}}" reservation-id={{$reservationId}}>
    <p class="d-flex align-items-center"><span>Pagamento tramite il circuito sicuro</span><i class="ml-2 fab fa-stripe fa-2x"></i></p>
    <div id="card-element" class="mb-3"><!--Stripe.js injects the Card Element--></div>
    <div class="mb-3">
        <input type="checkbox" value="1" name="stripe_accept" id="stripe_accept" style="height: auto !important; width: auto !important;" required>
        <label for="stripe_accept" class="d-inline ml-1">{{ __('Accetto termini e condizioni') }} - <a href="{{ route('privacy-policy') }}" target="_blank" class="bold text-dark">{{ __('Leggi qui') }}</label>
    </div>
    <button id="submit">
        <div class="spinner hidden" id="spinner"></div>
        <span id="button-text" class="text-uppercase"><i class="far fa-credit-card mr-2"></i>{{ __('Paga ora') }}</span>
    </button>
    <p id="card-error" role="alert"></p>
        <p class="result-message hidden">
        {{ __('Pagamento effettuato con successo. Controlla la tua') }}
        <a href="" target="_blank">{{ __('Stripe dashboard.') }}</a> {{ __('Ricarica la pagina per effettuare un altro pagamento.') }}
    </p>
</form>