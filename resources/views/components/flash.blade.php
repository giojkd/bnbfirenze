@if(Session::has('error') && Session::get('error') == 'no-reservation')
<div class="alert alert-success alert-dismissable text-center" style="position: absolute; top: 70px; left: 5%; width: 90%; z-index: 99;">
    <a href="{{ route('rooms') }}" class="inherit bold ml-3">{{ __('Associa la tua camera') }}<i class="far fa-sign-in-alt ml-2"></i></a><span class="text-dark font-weight-bold"> o </span><br class="d-md-none"> <a href="{{ route('external-menus') }}" class="inherit bold text-success">{{ __('ordina colazione da esterno') }}<i class="far fa-utensils ml-2 mr-3"></i></a>
</div>
@endif