<nav id="sticky-navbar" class="navbar fixed-top navbar-expand-lg navbar-dark">
    <div class="container container-large">
        <a class="navbar-brand" href="{{ url('/') }}">
            <h2 class="h5 mb-0">
                <img src="/img/logo-white.svg" id="logo" class="img-fluid mr-2" width="25" alt="Logo Leyla's Club">
                <strong>{{ __('Leyla\'s Club') }}</strong></h2>
        </a>
        <button class="navbar-toggler my-2" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <i class="far fa-bars"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            @auth
            <div class="d-sm-none text-center pt-4">
                <div class="d-smart-none rounded-circle mx-auto" style="height: 75px; width: 75px; background-color:rgb(231, 142, 157);">
                    <i class="fas fa-user-circle text-primary" style="font-size: 5rem;"></i>
                </div>
                <h4 class="my-2">{{Auth::user()->name . ' ' . Auth::user()->surname}}</h4>
                <hr>
            </div>
            @endauth

            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto bold">
                @if(Auth::user() && !Auth::user()->isChef())
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('menus') }}">{{ __('I menu') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('rooms') }}">{{ __('Le camere') }}</a>
                </li>
                @endif

                <!-- Sito web -->
                <li class="nav-item">
                    <a class="nav-link" href="https://www.lamarmora39.com/" target="_blank">{{ __('B&B La Marmora 39') }}</a>
                </li>

                <!-- Contatti -->
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('contact-us') }}">{{ __('Contatti') }}</a>
                </li>

                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Accedi') }}</a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Registrati') }}</a>
                        </li>
                    @endif
                @else
                    <!-- Icona carrello -->
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('user.orders') }}">
                            <i class="far fa-shopping-basket mr-1 ml-lg-2"></i><sup>{{ Auth::user()->trays }}</sup>
                            <span class="d-lg-none">{{ __('Carrello') }}</span>
                        </a>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            @if(Auth::user()->isAdmin())
                            <a class="dropdown-item" href="/admin">
                                {{ __('Gestisci') }}
                            </a>
                            @endif
                            @if(Auth::user() && !Auth::user()->isChef())
                            <a class="dropdown-item" href="{{ route('user.restricted-area') }}">
                                {{ __('Area riservata') }}
                            </a>
                            <a class="dropdown-item" href="{{ route('user.orders') }}">
                                {{ __('I miei ordini') }}
                            </a>
                            @endif
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Esci') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest

                <!-- Seleziona lingua -->
                <li class="nav-item dropdown">
                    <a id="languageDropdown" class="nav-link dropdown-toggle text-uppercase" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ config('app.locale') }}
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="languageDropdown">
                        <a class="dropdown-item" href="{{ route('choose.lang', 'it') }}">
                            IT
                        </a>
                        <a class="dropdown-item" href="{{ route('choose.lang', 'en') }}">
                            EN
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
