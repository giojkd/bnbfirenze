<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$title}}</title>
    <meta name="description" content="{{ $description }}">

    <!-- Fontawesome -->
    <script src="https://kit.fontawesome.com/b5939ad040.js" crossorigin="anonymous" defer></script>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    <livewire:styles />
    @stack('styles')
</head>
<body>
    <div id="app">
        <x-navbar />
        <x-flash />
        <main>
            {{ $slot }}
        </main>
        <x-footer />
    </div>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}"></script>

    @stack('scripts')

    <livewire:scripts />

    <script>
        // Navbar scroll
        document.addEventListener('DOMContentLoaded', function(){
            let sticky_navbar = document.querySelector('#sticky-navbar');

            if( sticky_navbar && (window.pageYOffset > 30 || screen.width <= 992) )
            {
                sticky_navbar.classList.remove('navbar-dark');
                sticky_navbar.classList.add('bg-light', 'navbar-light', 'shadow-sm');
                document.querySelector('#logo').setAttribute('src', '/img/logo-black.svg');
            }

            document.addEventListener('scroll', function(){
                if( sticky_navbar && (window.pageYOffset > 30 || screen.width <= 992) )
                {
                    sticky_navbar.classList.remove('navbar-dark');
                    sticky_navbar.classList.add('bg-light', 'navbar-light', 'shadow-sm');
                    document.querySelector('#logo').setAttribute('src', '/img/logo-black.svg');
                    sticky_navbar.style.transition = ".5s";
                } else {
                    sticky_navbar.classList.remove('bg-light', 'navbar-light', 'shadow-sm');
                    sticky_navbar.classList.add('navbar-dark');
                    document.querySelector('#logo').setAttribute('src', '/img/logo-white.svg');
                    sticky_navbar.style.transition = ".5s";
                }
            });

        });
    </script>

    @if(config('app.env') != 'local')
    <!-- Iubenda Cookie Policy -->
    <script type="text/javascript">
    var _iub = _iub || [];
    _iub.csConfiguration = {"whitelabel":false,"lang":"it","siteId":2198133,"perPurposeConsent":true,"gdprAppliesGlobally":false,"cookiePolicyId":20649713, "banner":{ "acceptButtonDisplay":true,"customizeButtonDisplay":true,"position":"float-bottom-center" }};
    </script>
    <script type="text/javascript" src="//cdn.iubenda.com/cs/iubenda_cs.js" charset="UTF-8" async></script>
    @endif

</body>
</html>
