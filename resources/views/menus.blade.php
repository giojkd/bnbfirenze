<x-layouts.app
    title="Scegli i menu"
    description="Questa è la descrizione per google"
>

@push('styles')

@endpush

<div class="container-fluid offset-navbar bg-warning">

    <header class="row align-items-center vh-30">

        <div class="col-12 text-white text-center">

            <h1 class="bold text-shadowed">{{ __('I nostri menu') }}</h1>

        </div>

    </header>

</div>

<div class="container pt-4 container-large">

    <div class="row justify-content-center py-5">
        @foreach ($customMenus as $customMenu)
            <div class="col-12 col-md-6 col-lg-4 mb-5">
                <!-- Menu alla carta -->


                    <div class="card h-100 border-0 rounded-0 shadow" style="height: 240px">
                        <img src="/img/menu-alla-carta.jpg" class="img-fluid" alt="Menu alla carta">
                        <div class="card-body">
                            <h3 class="font-weight-bold d-flex align-items-center justify-content-center text-center">
                                {{ $customMenu->title ?: $customMenu->name }}
                            </h3>
                            @if ($customMenu->description)
                            <div class="py-2 px-3">{!! $customMenu->content ?: $customMenu->description !!}</div>
                            @else
                            <p class="text-center py-2 px-3">
                                {!! __('<strong>Elenco completo dei piatti disponibili</strong> tutti preparati espressi dallo Chef.') !!}
                            </p>
                            @endif
                            @if ($customMenu->price)
                            <div class=" px-3 py-2 mt-auto font-weight-bold text-success">€ {{$customMenu->price}}</div>
                            @endif
                        </div>
                        @if($currentReservation->trays->where('menu_id', 1)->first())
                        <a href="#" disabled class="disabled btn btn-block btn-success rounded-0 text-uppercase bold mt-auto py-4">{{ __('Menu alla carta aggiunto') }}</a>
                        @else
                        <a href="{{ $customMenu->url() }}" class="btn btn-block btn-success rounded-0 text-uppercase bold mt-auto py-4"><i class="far fa-plus-circle mr-2"></i>{{ __('Aggiungi menu alla carta') }}</a>
                        @endif
                    </div>


            </div>
        @endforeach
        <div class="col-12 col-md-6 col-lg-4 mb-5">
            <!-- Menu fissi -->
            <div class="card h-100 border-0 rounded-0 shadow">
                <img src="/img/menu-fissi.jpg" class="img-fluid" alt="{{ __('Menu fissi') }}">
                @if($fixedMenu->count() > 0)
                <div class="card-body">
                    <h3 class="bold text-center">
                        {{ __('Menu fissi') }}
                    </h3>
                    <p class="text-center py-2 px-3">
                        {!! __('<strong>5 menu in offerta speciale</strong> le combinazioni dei piatti più richiesti preparati espressi dallo Chef') !!}
                    </p>
                </div>
                <a href="{{ route('fixed-menus') }}" class="btn btn-block btn-danger rounded-0 text-uppercase bold mt-auto py-4"><i class="far fa-plus-circle mr-2"></i>{{ __('Aggiungi menu fisso') }}</a>
                @else
                <div class="card-body">
                    <h3 class="bold text-center">
                        {{ __('Menu fissi') }}
                    </h3>
                    <p class="text-center py-2 px-3">
                        {!! __('Menu Fissi momentaneamente non disponibili, ordinare dal menu alla carta') !!}
                    </p>
                </div>
                <a href="#" class="btn btn-block disable btn-danger rounded-0 text-uppercase bold mt-auto py-4"><i class="far fa-ban mr-2"></i>{{ __('non disponibili') }}</a>
                @endif
            </div>
        </div>

        <div class="col-12 col-lg-8">

            @if($currentReservation->room)
            <p>{!! __('<strong>N.B.:</strong> Con il tuo profilo potrai effettuare ogni giorno un <strong>ordine unico per tutti gli ospiti della camera</strong>.') !!}</p>

            <p class="mb-5">
                {!! __('L\'ordine potrà contenere sia piatti scelti dal menu alla carta (utilizzando il budget giornaliero) che un qualsiasi numero di menu fissi che preferite, al prezzo indicato in ogni menu. Ogni camera dispone di <strong>un solo ordine al giorno</strong>. Potrai ordinare nuovamente la colazione dopo le ore 13.00.') !!}
            </p>
            @else
            <p class="mb-5">
                {!! __('L\'ordine potrà contenere sia piatti scelti dal menu alla carta che un qualsiasi numero di menu fissi, al prezzo indicato in ogni menu.') !!}
            </p>
            @endif

            <p class="text-center">
                <a href="{{ route('user.orders') }}" class="btn btn-dark rounded-0 text-uppercase bold mt-auto py-4 px-5">{{ __('I miei ordini') }}</a>
            </p>

        </div>

    </div>

</div>


@push('scripts')

@endpush

</x-layouts.app>
