<x-layouts.app
    title="Grazie"
    description="Questa è la descrizione per google"
>

@push('styles')

@endpush

<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-30">

        <div class="col-12 text-white text-center"> 

            <h1 class="bold text-shadowed">{{ __('Grazie') }}, {{ Auth::user()->name }}</h1>

        </div>

    </header>

</div>

<div class="container pt-4">

    <section class="row justify-content-center py-5">

        <div class="col-12 col-lg-6 text-center">

            <p class="lead">{{ __('La tua richiesta è stata inserita nel sistema. Riceverai una e-mail di conferma della tua prenotazione al più presto.')  }}</p>

        </div>

    </section>

    <hr>

    <section class="row justify-content-center py-5">

        <div class="col-12 text-center mb-5">

            <h2>{{ __('La Marmora 39') }}</h2>
            <p class="text-secondary">- {{ __('Bed & Breakfast') }} -</p>

        </div>

        <div class="col-12 col-lg-4 text-center mb-5">

            <h2 class="h1"><i class="far fa-phone"></i></h2>
            <p class="text-secondary">- 333 2292785 -</p>

        </div>

        <div class="col-12 col-lg-4 text-center mb-5">

            <h2 class="h1"><i class="far fa-envelope-open"></i></h2>
            <p class="text-secondary">- bblamarmora39@gmail.com -</p>

        </div>

        <div class="col-12 col-lg-4 text-center mb-5">

            <h2 class="h1"><i class="fab fa-facebook"></i></h2>
            <p class="text-secondary">- @bblamarmora39 -</p>

        </div>

        <div class="col-12 col-md-6 col-xl-4 text-center mb-5">

            <a href="tel:00393332292785" class="btn btn-dark btn-block rounded-0 border-0 text-uppercase bold px-5 py-3">Chiama ora</a>

        </div>

    </section>

</div>

<footer class="container-fluid">

    <hr class="border-secondary">

    <div class="container">

        <div class="row py-4">

            <div class="col-12 text-center">

                <p>
                    {{ __('B&B La Marmora 39, di AQSKINS Srl - Via Alfonso La Marmora, 39 50121 Firenze - P.I.: 06878700480 - ') }}
                    <a href="{{ route('privacy-policy') }}" target="_blank" class="inherit">
                        {{ __('Privacy Policy')}}
                    </a>
                </p>

            </div>

        </div>

    </div>

</footer>

@push('scripts')
    
@endpush

</x-layouts.app>