<x-layouts.app
    title="Leyla's Club - B&B Firenze"
    description="Prenota la colazione durante il tuo soggiorno nelle splendide stanze del B&B La Marmora 39 a Firenze"
>

@push('styles')

@endpush

<!-- Above the fold -->
<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-home py-5">

        <div class="col-12 text-center text-white text-shadowed">

            <img src="/img/logo-white.svg" alt="Leyla's Club" width="50" class="img-fluid mb-4">

            <h1 class="display-5 mb-4">Leyla's Club</h1>

            <h4 class="mb-4">{{ __('Colazione espressa') }}.<br class="d-md-none"> {{ __('Preparata con amore ogni mattina') }}</h4>

        </div>

    </header>

    <div class="row">
        <div class="col-12 px-0">
            <img src="/img/home-bottom-divider.svg" alt="Home Bottom Divider" class="img-fluid" width="100%" style="margin-bottom: -1px;">
        </div>
    </div>

</div>

<!-- Contenuto -->
<div class="container-fluid">

    <div class="container">

        <section class="row py-5">

            <article class="col-12 text-center">

                <p class="lead">{!! __('Leyla\'s Club è il sistema facile e veloce di <strong>B&B La Marmora 39</strong> per <strong>ordinare la colazione</strong>.') !!}</p>

                <p class="lead mb-5">
                    {!! __('Puoi scegliere tra <strong>5 Menu Fissi</strong>, in offerta speciale, e un <strong>Menu alla carta</strong> con l\'elenco completo dei piatti. Ogni giorno avrai a disposizione un <strong>budget colazione</strong> da spendere sul menu alla carta.') !!}
                </p>

                @guest
                <p><a href="{{ route('login') }}" class="btn btn-lg btn-dark px-5 rounded-0 text-uppercase">{{ __('Accedi') }}</a></p>
                @else
                <p><a href="{{ route('user.restricted-area') }}" class="btn btn-lg btn-dark px-5 rounded-0 text-uppercase">{{ __('Area riservata') }}</a></p>
                @endguest

            </article>

        </section>

    </div>

</div>

<footer class="container-fluid">

    <hr class="border-secondary">

    <div class="container">

        <div class="row py-4">

            <div class="col-12 text-center">

                <p>
                    {{ __('B&B La Marmora 39, di AQSKINS Srl - Via Alfonso La Marmora, 39 50121 Firenze - P.I.: 06878700480 - ') }}
                    <a href="{{ route('privacy-policy') }}" target="_blank" class="inherit">
                        {{ __('Privacy Policy')}}
                    </a>
                </p>

            </div>

        </div>

    </div>

</footer>

@push('scripts')
    
@endpush

</x-layouts.app>