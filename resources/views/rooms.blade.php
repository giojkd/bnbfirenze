<x-layouts.app
    title="Camere"
    description="Questa è la descrizione per google"
>

@push('styles')
    <style>
        
    </style>
@endpush

<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-40">

        <div class="col-12 text-white text-center"> 

            <h1 class="bold text-shadowed">{{ __('Le camere') }}</h1>

        </div>

    </header>

</div>

<div class="container container-large mt-5">
    <div class="row py-5 justify-content-center">
        @foreach ($rooms as $room)
        <div class="col-12 col-md-6 col-xl-4 mb-5">
            <div class="card h-100 border-0 rounded-0 shadow">
                <figure class="mb-0">
                    <img src="{{ $room->getFirstMediaUrl('gallery', 'thumb') }}" alt="{{ $room->title ?: $room->name }}" width="600" class="img-fluid">
                </figure>
                <div class="card-body px-4">                    
                    <h3 class="bold">{{ $room->title ?: $room->name }}</h3>
                    <p class="mb-0">{!! $room->getPreview() !!}</p>
                </div>  
                <a href="{{ $room->url() }}" class="btn btn-block btn-success rounded-0 text-uppercase bold mt-auto py-4">{{ __('Scopri di più') }}</a>
            </div>
        </div>
        @endforeach
    </div>
</div>

@push('scripts')
    
@endpush

</x-layouts.app>