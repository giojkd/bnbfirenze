<x-layouts.app
    title="Contattaci"
    description="Contatta il B&B La Marmora 39 a Firenze"
>

@push('styles')
    <style>
        
    </style>
@endpush

<div class="container-fluid offset-navbar bg-leyla">

    <header class="row align-items-center vh-40">

        <div class="col-12 text-white text-center"> 

            <h1 class="bold text-shadowed">{{ __('Contatti') }}</h1>

        </div>

    </header>

</div>

<div class="container pt-4">

    <section class="row justify-content-center py-5">

        <div class="col-12 text-center mb-5">

            <figure>
                <img src="/img/logo-bnb-la-marmora-39.svg" alt="B&B La Marmora, 39 (Firenze)" class="img-fluid" width="250">
            </figure>

        </div>

        <div class="col-12 col-lg-4 text-center mb-5">

            <figure>
                <img src="/img/phone.svg" alt="B&B La Marmora, 39 (Firenze)" class="img-fluid" width="50">
            </figure>
            <p class="text-secondary">- 333 2292785 -</p>

        </div>

        <div class="col-12 col-lg-4 text-center mb-5">

            <figure>
                <img src="/img/email.svg" alt="B&B La Marmora, 39 (Firenze)" class="img-fluid" width="50">
            </figure>
            <p class="text-secondary">- bblamarmora39@gmail.com -</p>

        </div>

        <div class="col-12 col-lg-4 text-center mb-5">

            <figure>
                <img src="/img/social.svg" alt="B&B La Marmora, 39 (Firenze)" class="img-fluid" width="110">
            </figure>
            <p class="text-secondary">- @bblamarmora39 -</p>

        </div>

        <div class="col-12 col-md-6 col-xl-4 text-center mb-5">

            <a href="tel:00393332292785" class="btn btn-dark btn-block rounded-0 border-0 text-uppercase bold px-5 py-3">{{ __('Chiama ora') }}</a>

        </div>

    </section>

    <hr>

    <section class="row justify-content-center py-5">

        <div class="col-12 col-lg-6 text-center">

            <p class="lead">{!! __('Leyla\'s Club è il servizio per la prenotazione della tua colazione espressa, offerto dal B&B La Marmora, 39 (Firenze)') !!}</p>

        </div>

    </section>

</div>

<footer class="container-fluid">

    <hr class="border-secondary">

    <div class="container">

        <div class="row py-4">

            <div class="col-12 text-center">

                <p>
                    {{ __('B&B La Marmora 39, di AQSKINS Srl - Via Alfonso La Marmora, 39 50121 Firenze - P.I.: 06878700480 - ') }}
                    <a href="{{ route('privacy-policy') }}" target="_blank" class="inherit">
                        {{ __('Privacy Policy')}}
                    </a>
                </p>

            </div>

        </div>

    </div>

</footer>

@push('scripts')
    
@endpush

</x-layouts.app>