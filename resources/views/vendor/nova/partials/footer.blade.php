<p class="mt-8 text-center text-xs text-80">
    &copy; {{ date('Y') }} B&B La Marmora 39, di AQSKINS Srl
    <span class="px-1">&middot;</span>
    Powered by <a href="https://aulab.it" target="_blank" class="text-primary dim no-underline">Aulab Srl</a>
    <span class="px-1">&middot;</span>
    Laravel Nova v{{ \Laravel\Nova\Nova::version() }}
</p>
