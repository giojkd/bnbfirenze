<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {

            $table->id();

            $table->foreignId('reservation_id')->constrained()->onDelete('cascade');
            $table->string('code');#->unique();
            $table->unsignedDecimal('total', 8, 2)->nullable();
            $table->date('day')->nullable();
            $table->string('slot')->nullable();
            $table->boolean('room_service')->default(false);
            $table->boolean('ready')->default(false);

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
