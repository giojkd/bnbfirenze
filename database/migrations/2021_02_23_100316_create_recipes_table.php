<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecipesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipes', function (Blueprint $table) {
            $table->id();

            $table->foreignId('category_id')->nullable()->constrained()->onDelete('SET NULL');

            $table->string('name');
            $table->string('type')->nullable();
            $table->text('description')->nullable();

            $table->unsignedDecimal('price', 8, 2)->nullable();
            $table->boolean('starred')->default(false);

            $table->boolean('published')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recipes');
    }
}
