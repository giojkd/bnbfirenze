<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Menu;
use App\Models\Recipe;

class MenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Menu alla carta
        $menu = Menu::create([
            "name" => "Menu alla carta",
            "description" => NULL,
            "price" => NULL,
            "color" => "#d2a152",
            "published" => true
        ]);
        $recipe_ids = [];
        foreach(Recipe::all() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        $menu->recipes()->attach($recipe_ids);



        // Healty
        $menu = Menu::create([
            "name" => "Menu Salutare",
            "description" => "<p><strong>1 Pezzo di Caffetteria</strong></p><hr><p><strong>1 Yogurt</strong></p><hr><p><strong>1 Frullato o succo di frutta</strong></p><hr><p><strong>1 Avocado Toast</strong></p>",
            "price" => 7,
            "color" => "#00973A",
            "published" => true
        ]);

        $menu->addMedia('public/img/loghi-menu/healty.svg')->preservingOriginal()->toMediaCollection('gallery');

        $recipe_ids = [];
        foreach(Recipe::where('type', 'Caffetteria')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('type', 'Yogurt')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('type', 'Succhi')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('name', 'Avocado Toast')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        $menu->recipes()->attach($recipe_ids);
        

        // Royal
        $menu = Menu::create([
            "name" => "Menu Reale",
            "description" => "<p><strong>1 Pezzo di Caffetteria</strong></p><hr><p><strong>1 Omlette o uova strapazzate</strong></p><hr><p><strong>1 Frullato o succo di frutta</strong></p><hr><p><strong>1 Avocado Toast</strong></p>",
            "price" => 7.5,
            "color" => "#FAB900",
            "published" => true
        ]);

        $menu->addMedia('public/img/loghi-menu/royal.svg')->preservingOriginal()->toMediaCollection('gallery');

        $recipe_ids = [];
        foreach(Recipe::where('type', 'Caffetteria')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('type', 'Uova')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('type', 'Succhi')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('name', 'Avocado Toast')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        $menu->recipes()->attach($recipe_ids);


        // Baked
        $menu = Menu::create([
            "name" => "Menu al Forno",
            "description" => "<p><strong>1 Pezzo di Caffetteria</strong></p><hr><p><strong>1 Avocado al Forno</strong></p><hr><p><strong>1 Frullato o succo di frutta</strong></p><hr><p><strong>1 Crumble</strong></p>",
            "price" => 8,
            "color" => "#E95A0C",
            "published" => true
        ]);

        $menu->addMedia('public/img/loghi-menu/baked.svg')->preservingOriginal()->toMediaCollection('gallery');

        $recipe_ids = [];
        foreach(Recipe::where('type', 'Caffetteria')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('name', 'Avocado al Forno')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('type', 'Succhi')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('name', 'Crumble')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        $menu->recipes()->attach($recipe_ids);


        //Champion's
        $menu = Menu::create([
            "name" => "Menu dei Campioni",
            "description" => "<p><strong>1 Pezzo di Caffetteria</strong></p><hr><p><strong>1 Uova alla Benedict</strong></p><hr><p><strong>1 Frullato o succo di frutta</strong></p><hr><p><strong>1 Pancake</strong></p>",
            "price" => 9,
            "color" => "#1D70B7",
            "published" => true
        ]);

        $menu->addMedia('public/img/loghi-menu/champions.svg')->preservingOriginal()->toMediaCollection('gallery');

        $recipe_ids = [];
        foreach(Recipe::where('type', 'Caffetteria')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('name', 'Uova alla Benedict')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('type', 'Succhi')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('type', 'Pancake')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        $menu->recipes()->attach($recipe_ids);

        
        //Salmon
        $menu = Menu::create([
            "name" => "Menu Salmone",
            "description" => "<p><strong>1 Pezzo di Caffetteria</strong></p><hr><p><strong>1 Uova alla Benedict con Salmone</strong></p><hr><p><strong>1 Frullato o succo di frutta</strong></p><hr><p><strong>1 Salmone con Avocado</strong></p>",
            "price" => 10,
            "color" => "#F092BC",
            "published" => true
        ]);
        
        $menu->addMedia('public/img/loghi-menu/salmon.svg')->preservingOriginal()->toMediaCollection('gallery');

        $recipe_ids = [];
        foreach(Recipe::where('type', 'Caffetteria')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('name', 'Uova alla Benedict con Salmone')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('type', 'Succhi')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        foreach(Recipe::where('name', 'Avocado Toast al Salmone')->get() as $recipe) {
            $recipe_ids[] = $recipe->id;
        }
        $menu->recipes()->attach($recipe_ids);
    }
}
