<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\Reservation;
use Illuminate\Database\Seeder;

class ReservationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Reservation::create([
            'arrival' => Carbon::today(),
            'departure' => Carbon::today()->addDays(4),
            'adults' => 2,
            'children' => 1,
            'user_id' => 2,
            'room_id' => 1,
            'confirmed' => true,
            'gdpr' => true,
        ]);
        Reservation::create([
            'arrival' => Carbon::today(),
            'departure' => Carbon::today()->addDays(4),
            'adults' => 2,
            'children' => 1,
            'user_id' => 3,
            'room_id' => 2,
            'confirmed' => true,
            'gdpr' => true,
        ]);
    }
}
