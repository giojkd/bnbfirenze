<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Room;

class RoomsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $room = Room::create([
            "name" => "Camera Turchese",
            "description" => "<p>E' la stanza più particolare, con un carattere deciso che richiama allo stile industriale per via dei suoi elementi di arredo a muro in ferro battuto. La camera, che dispone di un letto King size, ha due ampie finestre che affacciano su un silenzioso e tranquillo cortile. Al suo interno, è presente anche una poltrona letto singola e un funzionale tavolino. L’ambiente, di 24 mq, ha un bagno interno privato finestrato dotato di vasca-doccia e termo arredo moderno.</p>",
            "published" => true
        ]);
        $room->addMedia('public/img/foto-camere/camera-turchese.jpg')->preservingOriginal()->toMediaCollection('gallery');

        $room = Room::create([
            "name" => "Camera Blu",
            "description" => "<p>La nostra camera più grande!<br> Una suite di 46 mq, adatta a famiglie, ad amici, ma anche a una coppia che vuole regalarsi un soggiorno ancora più confortevole. E’ composta da due camere separate e un bagno, accessibili da un corridoio privato.<br> La camera più piccola è dotata di divano letto apribile king size, scrivania, TV e armadio e può essere usata sia come camera da letto che come studio soggiorno.<br> La più grande, è un open space di 30 mq, dotata di letto matrimoniale king size con spalliera retro illuminata e regolabile, dietro la quale si apre una comoda cabina armadio.<br>Arricchisce l’ambiente, una poltrona letto singola con tavolino da caffè, dove potersi intrattenere con un buon bicchiere di vino ed un libro.<br> Aggiunge valore alla suite, la dimensione e la comodità del bagno, un luminoso spazio di circa 6 mq.<br> Alla suite è annesso anche un balcone privato che affaccia su una corte interna e silenziosa.</p>",
            "published" => true
        ]);
        $room->addMedia('public/img/foto-camere/camera-blu.jpg')->preservingOriginal()->toMediaCollection('gallery');

        $room = Room::create([
            "name" => "Camera Arancione",
            "description" => "<p>La nostra unica camera singola!<br> Colorata e moderna. Nonostante le sue dimensioni ridotte (10 mq) ha tutte le dotazioni necessarie per un soggiorno confortevole, sia breve che medio-lungo. E’ un rifugio perfetto per coloro che vogliono visitare Firenze da soli o vi si recano per lavoro. Il bagno è ampio e nuovo, con uno spazioso piatto doccia. Dalla stanza si accede anche ad un piccolo balcone privato che affaccia su un silenzioso cortile interno.</p>",
            "published" => true
        ]);
        $room->addMedia('public/img/foto-camere/camera-arancione.jpg')->preservingOriginal()->toMediaCollection('gallery');

        $room = Room::create([
            "name" => "Camera Rosa",
            "description" => "<p>Nella stanza Rosa ci si immerge in un’atmosfera romantica, data dallo stile del suo arredamento a “tinte rosa”. E’ la camera più apprezzata dalla nostra clientela femminile.<br> La spaziosa cabina armadio retroilluminata, la particolarità degli specchi e la cura dei dettagli dell’arredo, la rendono una camera dal gusto molto ricercato.<br> Le dimensioni della stanza sono di 28 mq e vi è annesso un piccolo balcone.</p>",
            "published" => true
        ]);
        $room->addMedia('public/img/foto-camere/camera-rosa.jpg')->preservingOriginal()->toMediaCollection('gallery');

        $room = Room::create([
            "name" => "Camera Gialla",
            "description" => "<p>La “Gialla” è un affascinante open space di 35 mq, luminoso e caratterizzato da elementi di arredo pop. È dotata di un balcone privato, doppia porta finestra, bagno interno non finestrato, e un comodo letto matrimoniale king size.<br> È l’ambiente ideale per le coppie e per i singoli viaggiatori che intendono godersi gli ampi spazi a disposizione. La presenza di un grande divano letto angolare, la rende comodamente abitabile anche per un maggior numero di ospiti. All’interno della stanza trovano posto altri elementi di arredamento, quali due poltrone e un tavolino da caffè.</p>",
            "published" => true
        ]);
        $room->addMedia('public/img/foto-camere/camera-gialla.jpg')->preservingOriginal()->toMediaCollection('gallery');

        $room = Room::create([
            "name" => "Camera Verde",
            "description" => "<p>La soluzione ideale per una famiglia!<br> La stanza Verde è, infatti, una quadrupla che si compone di due camere, separate da un piccolo disimpegno, sul quale si apre un bagno interno. La camera principale, è una matrimoniale di oltre 24 mq che dispone di un balcone interno esclusivo e di una spaziosa cabina armadio. Il letto, king size, dispone di una particolare spalliera retroilluminata, dall’intensità regolabile, che, grazie ai colori tenui della stanza, dona un effetto romantico e rilassante. A questa, si aggiunge una camera singola dotata di letto alla francese (1,40×1,90m) finemente arredata con elementi unici realizzati a mano. La camera, grazie alla distribuzione degli spazi, è ideale per ospitare da 2 a 4 persone.</p>",
            "published" => true
        ]);
        $room->addMedia('public/img/foto-camere/camera-verde.jpg')->preservingOriginal()->toMediaCollection('gallery');

    }
}
