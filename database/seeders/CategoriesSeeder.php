<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $category = Category::create([
            "name" => "Caffetteria",
            "published" => true
        ]);
        $category->addMedia('public/img/icone-categorie/caffetteria.svg')->preservingOriginal()->toMediaCollection('picture');
        
        $category = Category::create([
            "name" => "Succhi",
            "published" => true
        ]);
        $category->addMedia('public/img/icone-categorie/succhi-frullati.svg')->preservingOriginal()->toMediaCollection('picture');
        
        $category = Category::create([
            "name" => "Dolci",
            "published" => true
        ]);
        $category->addMedia('public/img/icone-categorie/dolci.svg')->preservingOriginal()->toMediaCollection('picture');
        
        $category = Category::create([
            "name" => "Toast",
            "published" => true
        ]);
        $category->addMedia('public/img/icone-categorie/toast.svg')->preservingOriginal()->toMediaCollection('picture');
        
        $category = Category::create([
            "name" => "Uova",
            "published" => true
        ]);
        $category->addMedia('public/img/icone-categorie/uova.svg')->preservingOriginal()->toMediaCollection('picture');
        
    }
}
