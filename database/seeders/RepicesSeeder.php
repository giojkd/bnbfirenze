<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Recipe;

class RepicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Caffetteria
        Recipe::create([
            "category_id" => 1,
            "name" => "Espresso Monorigine",
            "type" => "Caffetteria",
            "description" => NULL,
            "price" => 1.5,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 1,
            "name" => "Caffe e Latte",
            "type" => "Caffetteria",
            "description" => NULL,
            "price" => 2.5,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 1,
            "name" => "Cappuccino",
            "type" => "Caffetteria",
            "description" => NULL,
            "price" => 2.5,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 1,
            "name" => "Cappuccino con latte di Soia",
            "type" => "Caffetteria",
            "description" => NULL,
            "price" => 2.5,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 1,
            "name" => "Caffe Americano",
            "type" => "Caffetteria",
            "description" => NULL,
            "price" => 2.5,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 1,
            "name" => "The Caldo",
            "type" => "Caffetteria",
            "description" => NULL,
            "price" => 1.5,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 1,
            "name" => "Flat White",
            "type" => "Caffetteria",
            "description" => NULL,
            "price" => 3,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 1,
            "name" => "Latte Macchiato",
            "type" => "Caffetteria",
            "description" => NULL,
            "price" => 3,
            "starred" => true,
            "published" => true
        ]);

        // Succhi
        
        Recipe::create([
            "category_id" => 2,
            "name" => "Succo di pomodoro",
            "type" => "Succhi",
            "description" => NULL,
            "price" => 3,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 2,
            "name" => "Succo all'Ananas",
            "type" => "Succhi",
            "description" => NULL,
            "price" => 2.5,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 2,
            "name" => "Succo all'Arancia Rossa",
            "type" => "Succhi",
            "description" => NULL,
            "price" => 2.5,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 2,
            "name" => "Succo alla Pesca",
            "type" => "Succhi",
            "description" => NULL,
            "price" => 2.5,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 2,
            "name" => "Succo all'Albicocca",
            "type" => "Succhi",
            "description" => NULL,
            "price" => 2.5,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 2,
            "name" => "Succo di Mirtillo",
            "type" => "Succhi",
            "description" => NULL,
            "price" => 3,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 2,
            "name" => "Frullato",
            "type" => "Succhi",
            "description" => NULL,
            "price" => 5,
            "starred" => false,
            "published" => true
        ]);

        // Dolci
        
        Recipe::create([
            "category_id" => 3,
            "name" => "Cornetto",
            "type" => "Croissant",
            "description" => NULL,
            "price" => 2,
            "starred" => true,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 3,
            "name" => "Yogurt con Muesli",
            "type" => "Yogurt",
            "description" => NULL,
            "price" => 5,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 3,
            "name" => "Yogurt con Frutta Fresca",
            "type" => "Yogurt",
            "description" => NULL,
            "price" => 5,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 3,
            "name" => "Yogurt con Miele e Noci",
            "type" => "Yogurt",
            "description" => NULL,
            "price" => 5.5,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 3,
            "name" => "Pancake",
            "type" => "Pancake",
            "description" => NULL,
            "price" => 5.5,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 3,
            "name" => "Pancake Ganache",
            "type" => "Pancake",
            "description" => "Nutella e Panna montata",
            "price" => 6,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 3,
            "name" => "Pancake al Gelato",
            "type" => "Pancake",
            "description" => NULL,
            "price" => 5.5,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 3,
            "name" => "Pancake allo Sciroppo d'acero",
            "type" => "Pancake",
            "description" => NULL,
            "price" => 5.5,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 3,
            "name" => "Pancake alla Fragola e Panna Montata",
            "type" => "Pancake",
            "description" => NULL,
            "price" => 6,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 3,
            "name" => "Crumble",
            "type" => "Torte",
            "description" => NULL,
            "price" => 7,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 3,
            "name" => "Kaiserschmarren mirtillo",
            "type" => "Torte",
            "description" => "Frittata dolce con Mele e Uva passa, servito con mirtillo rosso selvatico ",
            "price" => 7,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 3,
            "name" => "Kaiserschmarren mele",
            "type" => "Torte",
            "description" => "Frittata dolce con Mele e Uva passa, servito con puera di mele ",
            "price" => 7,
            "starred" => false,
            "published" => true
        ]);

        // Toast
        
        Recipe::create([
            "category_id" => 4,
            "name" => "Prosciutto e Formaggio",
            "type" => "Salato",
            "description" => NULL,
            "price" => 4,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 4,
            "name" => "French Toast al Prosciutto",
            "type" => "French Toast",
            "description" => NULL,
            "price" => 4,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 4,
            "name" => "French Toast alla Nutella",
            "type" => "French Toast",
            "description" => NULL,
            "price" => 4,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 4,
            "name" => "Avocado Toast",
            "type" => "Avocado",
            "description" => NULL,
            "price" => 5.5,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 4,
            "name" => "Avocado Toast con Uova e Bacon",
            "type" => "Avocado",
            "description" => NULL,
            "price" => 6.5,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 4,
            "name" => "Avocado Toast al Salmone",
            "type" => "Avocado",
            "description" => NULL,
            "price" => 7,
            "starred" => false,
            "published" => true
        ]);

        // Uova
        
        Recipe::create([
            "category_id" => 5,
            "name" => "Uova in Camicia",
            "type" => "Uova",
            "description" => "Su letto verde con scaglie di formaggio stagionato",
            "price" => 3,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 5,
            "name" => "Uova Strapazzate con Bacon",
            "type" => "Uova",
            "description" => NULL,
            "price" => 6.5,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 5,
            "name" => "Omelette con Prosciutto e Formaggio",
            "type" => "Uova",
            "description" => NULL,
            "price" => 6.5,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 5,
            "name" => "Uova alla Benedict",
            "type" => "Uova",
            "description" => "Singolo uovo in camicia con salsa olandese",
            "price" => 7,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 5,
            "name" => "Uova alla Benedict con Salmone",
            "type" => "Uova",
            "description" => "Singolo uovo in camicia con salsa olandese",
            "price" => 7.5,
            "starred" => false,
            "published" => true
        ]);
        Recipe::create([
            "category_id" => 5,
            "name" => "Avocado al Forno",
            "type" => "Avocado",
            "description" => "Servito con uova, formaggio e bacon croccante",
            "price" => 7.5,
            "starred" => false,
            "published" => true
        ]);
    }
}
