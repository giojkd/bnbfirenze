<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "Aulab",
            "surname" => "Srl",
            "email" => "aulab@aulab.it",
            "phone" => "3926024621",
            "password" => bcrypt("12345678"),
            "role" => "admin"
        ]);
        User::create([
            "name" => "Paolo",
            "surname" => "Caccavo",
            "email" => "caccavo.paolo@gmail.com",
            "phone" => "3333619849",
            "password" => bcrypt("12345678"),
            "role" => "admin"
        ]);
        User::create([
            "name" => "Leonardo",
            "surname" => "De Candia",
            "email" => "decandia.leonardo95@gmail.com",
            "phone" => "3287575595",
            "password" => bcrypt("12345678"),
            "role" => "admin"
        ]);
        User::create([
            "name" => "Chef",
            "surname" => "Tony",
            "email" => "prova.chef@gmail.com",
            "phone" => "3333333333",
            "password" => bcrypt("12345678"),
            "role" => "chef"
        ]);
        User::create([
            "name" => "Visitatore",
            "surname" => "Qualsiasi",
            "email" => "prova.visitatore@gmail.com",
            "phone" => "3331234567",
            "password" => bcrypt("12345678"),
            "role" => "user"
        ]);
    }
}
